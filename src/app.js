'use strict';

angular.module('constants', []);
angular.module('directives', ['ipCookie', 'ngStorage', 'resultFactoryModule', 'introFactoryModule']);
angular.module('bookmarkServiceModule', ['ngStorage']);
angular.module('resultFactoryModule', ['ngStorage']);
angular.module('introFactoryModule', ['ngStorage']);
angular.module('menuServiceModule', ['resultFactoryModule']);
angular.module('questionServiceModule', ['constants']);
angular.module('navigationServiceModule', ['questionServiceModule']);

angular.module('ngApp',
  ['ngRoute',
    'ngAria',
    'ngSanitize',
    'ngStorage',
    'config',
    'directives',
    'navigationServiceModule',
    'menuServiceModule',
    'angularLoad',
    'angular-google-analytics',
    'introFactoryModule',
    'bookmarkServiceModule'])

  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'components/intro/start.html'
      })
      .when('/start', {
        templateUrl: 'components/intro/start.html'
      })
      .when('/intro', {
        templateUrl: 'components/intro/intro.html'
      })
      .when('/intro/info', {
        templateUrl: 'components/intro/info.html'
      })
      .when('/menu', {
        templateUrl: 'components/menu/menu-fullpage.html'
      })
      .when('/result', {
        templateUrl: 'components/result/result.html'
      })
      .when('/questions', {
        templateUrl: 'components/questions/questions.html'
      })
      .when('/cookieinfo', {
        templateUrl: 'components/cookie-info/cookie-info.html'
      })
      .when('/question/:number', {
        templateUrl: 'components/questions/questions.html'
      })
      .when('/info/:number', {
        templateUrl: function (params) {
          return 'components/questions/' + params.number + '/info.html';
        }
      })
      .when('/controlquestions/', {
        templateUrl: 'components/questions/shared/controlquestions/control-questions.html'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .config(function (AnalyticsProvider, ENV_CONFIG) {
    AnalyticsProvider.setAccount(ENV_CONFIG.gaAccount);
    AnalyticsProvider.setDomainName(ENV_CONFIG.baseUrl);
    AnalyticsProvider.trackPages(false);
  })

  .run(function ($location, $rootScope, $localStorage, ENV_CONFIG) {
    $rootScope.version = ENV_CONFIG.version;
    $rootScope.timestamp = ENV_CONFIG.timestamp;
  })

  .run(function(IntroFactory) {
    IntroFactory.loadIntroFromStorage();
  })

  .run(function(ResultFactory) {
    ResultFactory.loadQuestionsFromStorage();
  })

  .run(function($rootScope, BookmarkService) {
    $rootScope.$on('$locationChangeStart', function() {
      BookmarkService.setBookmark();
    });
  })

  .run(function($rootScope, $location, Analytics) {
    $rootScope.$on('$routeChangeSuccess', function () {
      Analytics.trackPage($location.$$path);
    });
  })

  .run(function(BookmarkService) {
    BookmarkService.openBookmark();
  });