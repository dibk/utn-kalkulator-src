'use strict';

angular.module('ngApp')
  .controller('Input-3-Controller', function ($scope, ResultFactory) {

    $scope.checked = ResultFactory.builtResidence === 0 &&
      ResultFactory.builtGarage === 0 &&
      ResultFactory.builtSmallBuilding === 0 &&
      ResultFactory.builtOther === 0;
    $scope.builtResidence = ResultFactory.builtResidence;
    $scope.builtGarage = ResultFactory.builtGarage;
    $scope.builtSmallBuilding = ResultFactory.builtSmallBuilding;
    $scope.builtOther = ResultFactory.builtOther;

    var titledCalcMethodDesc = $scope.calculationMethodDescription[0].toUpperCase() + $scope.calculationMethodDescription.slice(1);
    var byaSuffix = '';

    if ($scope.baseCalculationMethod === 'BYA') {
      titledCalcMethodDesc = 'Areal';
      byaSuffix = ' i BYA';
    }

    $scope.inputs = [
      {header: titledCalcMethodDesc + ' bolig' + byaSuffix, name: 'builtResidence', model: $scope.builtResidence},
      {header: titledCalcMethodDesc + ' frittstående garasje', name: 'builtGarage', model: $scope.builtGarage},
      {header: titledCalcMethodDesc + ' frittstående bod/uthus/dukkehus', name: 'builtSmallBuilding', model: $scope.builtSmallBuilding},
      {header: titledCalcMethodDesc + ' for annen frittstående konstruksjon', name: 'builtOther', model: $scope.builtOther}
    ];

    $scope.forms = {};

    $scope.resetValues = function () {
      $scope.inputs.forEach(function (input) {
        input.model = $scope.checked === true ? 0 : undefined;
        ResultFactory.setAndPersistProperty(input.name, input.model);
      });
    };

    $scope.inputChange = function (property, value) {
      $scope.updateResult(property, value);
      $scope.checked = false;
    };

  });
