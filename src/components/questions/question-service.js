'use strict';

angular.module('questionServiceModule')

  .service('QuestionService', function (QuestionFactory) {

    this.findByNumber = function (number) {
      // The questions in the questionsArray are numbered by their indices
      // in the array.
      return QuestionFactory.getQuestion(number);
      
    };

  });
