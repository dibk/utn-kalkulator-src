'use strict';

angular.module('ngApp')
  .controller('InfoController', function ($scope, $routeParams, $localStorage) {
    $scope.number = $routeParams.number;
    if($localStorage.unit === 'm2') {
      $scope.parkingNumber = $scope.number-2;
    }else{
      $scope.parkingNumber = $scope.number;
    }
    $scope.regulationTime = $localStorage.regulationTime;
  });
