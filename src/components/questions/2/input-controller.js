'use strict';

angular.module('ngApp')
  .controller('Input-2-Controller', function ($scope, ResultFactory) {

    $scope.regulatedToOther = ResultFactory.regulatedToOther;
    $scope.propertyArea = ResultFactory.propertyArea;

  });
