'use strict';

angular.module('navigationServiceModule')

  .service('NavigationService', function (QuestionService) {

    this.structuralNext = function (number) {
      var next = QuestionService.findByNumber(number).next;
      return isNaN(next) ? next : '/question/' + next;
    };

    this.structuralPrevious = function (number) {
      var previous = QuestionService.findByNumber(number).previous;
      return isNaN(previous) ? previous : '/question/' + previous;
    };

  });

