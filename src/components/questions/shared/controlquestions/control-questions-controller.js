'use strict';

angular.module('ngApp')
  .controller('ControlQuestionsController', function ($scope, $localStorage, QuestionFactory) {

    var regulationTime = $localStorage.regulationTime;
    var calcMethod = $localStorage.calculationMethod;
    var description = QuestionFactory.getDescription(calcMethod);

    $scope.calcMethod = calcMethod;
    $scope.baseCalculation = QuestionFactory.getBaseCalculationMethod(calcMethod);
    var baseCalculationFolder = $scope.baseCalculation.toLowerCase();

    if($localStorage.unit === 'm2') {
      $scope.number = $localStorage.currentQuestion-2;
    }else{
      $scope.number = $localStorage.currentQuestion;
    }

    $scope.controlQuestions =
    { 'bya':
      [
        {
          'title': 'Dette er ' + description,
          'include': 'components/questions/shared/controlquestions/' + baseCalculationFolder + '/' + regulationTime + '/build-areal-info.html',
          'anchor': 'dette-er-bebygd-areal'
        },
        {
          'include': 'components/questions/shared/controlquestions/open-build-areal.html'
        },
        {
          'title': 'Utkragede bygningsdeler',
          'include': 'components/questions/shared/controlquestions/' + baseCalculationFolder + '/' + regulationTime + '/projecting-parts.html',
          'anchor': 'utkragede-bygningsdeler'
        },
        {
          'title': 'Delene som er plassert over planert terreng',
          'include': 'components/questions/shared/controlquestions/' + baseCalculationFolder + '/' + regulationTime + '/parts-over-terrain.html',
          'anchor': 'delene-som-er-plassert-over-planert-terreng'
        }
      ],
      'bra':
      [
        {
          'title': 'Dette er ' + description,
          'include': 'components/questions/shared/controlquestions/' + baseCalculationFolder + '/' + regulationTime + '/bra.html',
          'anchor': 'dette-er-bruksareal'
        }
      ]
    };

  });
