'use strict';

angular.module('ngApp')
  .controller('QuestionHeaderController', function ($scope, QuestionService) {
    $scope.header = QuestionService.findByNumber($scope.number).header;
  });
