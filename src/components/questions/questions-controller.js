'use strict';

angular.module('ngApp')
  .controller('QuestionsController', function ($scope, $window, $routeParams, $location, $localStorage,
                                               NavigationService, ResultFactory, MenuService, QuestionFactory) {
     $scope.getActualQuestion = function() {
      if($localStorage.unit === 'm2') {
        return parseInt($routeParams.number || 1)+2;
      }
    }

    $scope.number = (function () {
      var nextNumber = $routeParams.number || 1;
      var fromNumber = $localStorage.currentQuestion;
      if($localStorage.unit === 'm2') {
        fromNumber -=2;
      }

      if($localStorage.unit === 'm2') {
        $localStorage.currentQuestion = $scope.getActualQuestion();
      }else{
        $localStorage.currentQuestion = nextNumber;
      }

      if (fromNumber && !MenuService.isAnswered(fromNumber) && nextNumber !== fromNumber) {
        MenuService.addSkippedQuestion(fromNumber);
      } else if (fromNumber && MenuService.isAnswered(fromNumber)) {
        MenuService.removeSkippedQuestion(fromNumber);
      }
      
      return $localStorage.currentQuestion;
    })();

    $window.scrollTo(0, 0);
    if($scope.number !== undefined && $scope.number !== null) {
      var currentQuestion = QuestionFactory.getQuestions()[parseInt($routeParams.number || 1)-1];
      if(currentQuestion) {
        $scope.bottomMenuText = currentQuestion.bottomMenuText;
      }
    }
    
   

    $scope.resolveNext = function (number) {
      $location.url(NavigationService.structuralNext(number));
    };

    $scope.resolvePrevious = function (number) {
      $location.url(NavigationService.structuralPrevious(number));
    };

    $scope.updateResult = function (property, value) {
      ResultFactory.setAndPersistProperty(property, value);
    };

    $scope.calculationMethod = $localStorage.calculationMethod;
    $scope.calculationMethodDescription = QuestionFactory.getDescription($scope.calculationMethod);
    $scope.baseCalculationMethod = QuestionFactory.getBaseCalculationMethod($scope.calculationMethod)

  });
