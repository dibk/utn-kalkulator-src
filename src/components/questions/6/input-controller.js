'use strict';

angular.module('ngApp')
  .controller('Input-6-Controller', function ($scope, ResultFactory, VARIABLES, $localStorage) {
    
    $scope.calculationMethod = $localStorage.calculationMethod;
    $scope.regulationPlan = $localStorage.regulationPlan;

    

    $scope.parkingPlaces = ResultFactory.parkingPlaces;
    $scope.parkingPlaceArea = ResultFactory.parkingPlaceArea;
    if($scope.parkingPlaceArea === undefined) {
      $scope.parkingPlaceArea = 18;
      $scope.updateResult('parkingPlaceArea', 18);
    }
    $scope.parkingPlacesGarage = ResultFactory.parkingPlacesGarage;
    $scope.requiredExtraTerrainParkingPlaces = ResultFactory.requiredExtraTerrainParkingPlaces;
    $scope.hasRequiredExtraTerrainParkingPlacesInRegPlan = ResultFactory.hasRequiredExtraTerrainParkingPlacesInRegPlan;

    $scope.parkingCalculation = function() {
      // recheck parking
      if($scope.parkingPlaces !== (undefined || null) && $scope.parkingPlacesGarage !== (undefined || null)) {
        if($scope.hasRequiredExtraTerrainParkingPlacesInRegPlan) {
          if($scope.requiredExtraTerrainParkingPlaces !== undefined && $scope.requiredExtraTerrainParkingPlaces !== null) {
            $scope.addedParkingPlaces = $scope.parkingPlaces-$scope.parkingPlacesGarage-$scope.requiredExtraTerrainParkingPlaces;
            }
        }else{
          $scope.addedParkingPlaces = $scope.parkingPlaces-$scope.parkingPlacesGarage;
         
        }
      }
    };

    // Set it to 0 initially
    //$scope.addedParkingPlaces = 0;
    // Run update on addedParkingPlaces to be correct when view is loaded
    $scope.parkingCalculation();

      $scope.inputs = [
        {
          header: 'Hvor mange parkeringsplasser kreves totalt på eiendommen?',
          name: 'parkingPlaces',
          model: $scope.parkingPlaces,
          unit: 'stk'
        },
        {
          header: 'Antall parkeringsplasser i garasje/carport',
          name: 'parkingPlacesGarage',
          model: $scope.parkingPlacesGarage,
          unit: 'stk'
        }
      ];

      $scope.extraParkingArea = {
          header: 'Sier parkeringsreglene for din eiendom noe om hvor stor hver parkeringsplass må være? Hvis det ikke står noe, er det 18 m2.',
          name: 'parkingPlaceArea',
          model: $scope.parkingPlaceArea,
          unit: 'm2'
      };
    

    $scope.inputChange = function (property, value) {
      $scope[property] = value;
      $scope.updateResult(property, value);
      if(property === 'hasRequiredExtraTerrainParkingPlacesInRegPlan') {
        if(value === false) {
          $scope['requiredExtraTerrainParkingPlaces'] = 0;
          $scope.updateResult('requiredExtraTerrainParkingPlaces', 0);
        }
      }
      $scope.parkingCalculation();
      
    };

    

  });
