'use strict';

angular.module('ngApp')
  .controller('Input-4-Controller', function ($scope, ResultFactory) {

    $scope.tearDownArea = ResultFactory.tearDownArea;
    $scope.existingArea = ResultFactory.builtGarage +
      ResultFactory.builtOther + 
      ResultFactory.builtResidence +
      ResultFactory.builtSmallBuilding;
      if($scope.existingArea === 0) {
      	$scope.tearDownArea = 0;
      	$scope.updateResult('tearDownArea', $scope.tearDownArea);
      }

  });
