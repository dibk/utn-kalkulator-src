'use strict';

angular.module('bookmarkServiceModule')

  .service('BookmarkService', function ($location, $localStorage) {

    this.key = 'currentBookmark';

    this.setBookmark = function () {
      $localStorage[this.key] = $location.url();
    };

    this.openBookmark = function () {
      var bookmark = $localStorage[this.key];
      if (!!bookmark) {
        $location.url($localStorage[this.key]);
      }
    };

  });