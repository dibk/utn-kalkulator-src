function QuestionFactory($localStorage, VARIABLES) {

  function getDescription(calcMethod) {
    switch (calcMethod) {
      case VARIABLES.KEYS.m2BYAKey:
      case VARIABLES.KEYS.pctBYAKey:
        return 'bebygd areal';
      case VARIABLES.KEYS.m2TBRAKey:
        return 'bruksareal';
      case VARIABLES.KEYS.pctTUKey:
        return 'bruksareal';
      case VARIABLES.KEYS.m2BRAKey:
      case VARIABLES.KEYS.pctBRAKey:
        return 'bruksareal';
      default:
        return '';
    }
  }

  function getBaseCalculationMethod(calcMethod) {
    switch (calcMethod) {
      case VARIABLES.KEYS.m2BYAKey:
      case VARIABLES.KEYS.pctBYAKey:
        return 'BYA';
      case VARIABLES.KEYS.m2TBRAKey:
      case VARIABLES.KEYS.pctTUKey:
      case VARIABLES.KEYS.m2BRAKey:
      case VARIABLES.KEYS.pctBRAKey:
        return 'BRA';
      default:
        return '';
    }
  }

  function getQuestions() {
    if($localStorage.unit === 'm2') {
      var questions = [
      {
        'number': 1,
        'header': 'Hvor mye av eiendommen din er allerede bebygd?',
        'previous': '/intro',
        'next': 2,
        'inputs': [
          {
            'key': 'builtResidence',
            'header': 'Grunnflate bolig'
          },
          {
            'key':'builtGarage',
            'header': 'Frittstående garasje'
          },
          {
            'key': 'builtSmallBuilding',
            'header': 'Frittstående bod/uthus/dukkehus'
          },
          {
            'key': 'builtOther',
            'header': 'Annen frittstående konstruksjon'
          }]
        },
        {
          'number': 2,
          'header': 'Hvor mye av dagens bebyggelse skal rives?',
          'previous': 1,
          'next': 3
        },
        {
          'number': 3,
          'header': 'Hvor stort blir det nye du skal bygge?',
          'previous': 2,
          'next': 4
        },
        {
          'number': 4,
          'header': 'Hvor stort parkeringsareal skal du ha på terreng på eiendommen din?',
          'previous': 3,
          'bottomMenuText' : 'Du finner som regel kravene som gjelder for din eiendom i planbestemmelsen du har funnet fram tidligere: Enten reguleringsplanen eller kommuneplanen. Du kan også sjekke om kommunen har en parkeringsnorm.',
          'next': '/result'
        }
      ];
    }else{
    var questions = [
      {
        'number': 1,
        'header': 'Hvor stor er eiendommen?',
        'previous': '/intro',
        'next': 2
      },
      {
        'number': 2,
        'header': 'Hvor stor del av eiendommen er regulert til noe annet enn bebyggelse?',
        'previous': 1,
        'next': 3
      },
      {
        'number': 3,
        'header': 'Hvor mye av eiendommen din er allerede bebygd?',
        'previous': 2,
        'next': 4,
        'inputs': [
          {
            'key': 'builtResidence',
            'header': 'Grunnflate bolig'
          },
          {
            'key':'builtGarage',
            'header': 'Frittstående garasje'
          },
          {
            'key': 'builtSmallBuilding',
            'header': 'Frittstående bod/uthus/dukkehus'
          },
          {
            'key': 'builtOther',
            'header': 'Annen frittstående konstruksjon'
          }]
        },
        {
          'number': 4,
          'header': 'Hvor mye av dagens bebyggelse skal rives?',
          'previous': 3,
          'next': 5
        },
        {
          'number': 5,
          'header': 'Hvor stort blir det nye du skal bygge?',
          'previous': 4,
          'next': 6
        },
        {
          'number': 6,
          'header': 'Hvor stort parkeringsareal må du ha?',
          'previous': 5,
          'bottomMenuText' : 'Du finner som regel kravene som gjelder for din eiendom i planbestemmelsen du har funnet fram tidligere: Enten reguleringsplanen eller kommuneplanen. Du kan også sjekke om kommunen har en parkeringsnorm.',
          'next': '/result'
        }
      ];
    }

      return questions;

    };

    var differences = [
      {
        'header': '',
        'calcMethod': {}
      },
      {
        'header': '',
        'calcMethod': {}
      },
      {
        'header': '',
        //'header2': 'Another Header',
        'calcMethod': {}
      },
      {
        'header': '',
        'calcMethod': {}
      },
      {
        'header': '',
        'calcMethod': {}
      },
      {
        'header': '',
        'calcMethod': {}
      },
      {
        'header': '',
        'calcMethod': {}
      }
    ];

    function getQuestion(number) {
      var i = parseInt(number) - 1;
      if($localStorage.unit === 'm2') {
        i -=2;
      }
      var question = getQuestions()[i];
      var changedHeader = differences[i][differences[i].calcMethod[$localStorage.calculationMethod]];
      question.header = changedHeader ? changedHeader : question.header;
      return question;
    };

    return {
      getQuestions : getQuestions,
      getQuestion : getQuestion,
      getDescription : getDescription,
      getBaseCalculationMethod : getBaseCalculationMethod
    };

  }

angular.module('constants')
.factory('QuestionFactory', QuestionFactory)
.constant('VARIABLES', (function() {
  var keys = {
    'KEYS' : {
      'm2BYAKey' : 'BYA',
      'pctBYAKey' : '%BYA',
      'm2TBRAKey' : 'T-BRA',
      'pctTUKey' : '%TU',
      'm2BRAKey' : 'BRA',
      'pctBRAKey' : '%BRA',
      'm2Key' : 'm2',
      'pctKey' : '%',
      'before2007Key' : 'before2007Regulation',
      'before1997Key': 'before1997Regulation',
      'parkingNormKey': 'parkeringsnorm'
    }
  };
  return keys;
}()));
