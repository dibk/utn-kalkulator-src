'use strict';

angular.module('menuServiceModule')

  .service('MenuService', function (ResultFactory, $localStorage) {
    if($localStorage.unit==='m2') {
      var inputs = [
        ['builtResidence', 'builtGarage', 'builtSmallBuilding', 'builtOther'],
        ['tearDownArea'],
        ['newRegistredArea'],
        ['parkingPlaces', 'parkingPlacesGarage', 'hasRequiredExtraTerrainParkingPlacesInRegPlan']
      ];
    }else{
      var inputs = [
        ['propertyArea'],
        ['regulatedToOther'],
        ['builtResidence', 'builtGarage', 'builtSmallBuilding', 'builtOther'],
        ['tearDownArea'],
        ['newRegistredArea'],
        ['parkingPlaces', 'parkingPlacesGarage', 'hasRequiredExtraTerrainParkingPlacesInRegPlan']
      ];
    }
    var inputsFlatten = [].concat.apply([], inputs);

    var skippedQuestions = [];

    this.addSkippedQuestion = function (number) {
      skippedQuestions[number] = true;
    };

    this.removeSkippedQuestion = function (number) {
      if (number in skippedQuestions) {
        skippedQuestions.splice(number, 1);
      }
    };

    this.questionSkipped = function (number) {
      return number in skippedQuestions;
    };

    this.clearSkippedQuestions = function() {
      skippedQuestions = [];
    };

    this.removeAllSkippedQuestions = function () {
      skippedQuestions = [];
    };

    this.isAnswered = function (number) {
      if($localStorage.unit==='m2') {
        switch(number) {
        case 1:
          return ResultFactory.question3IsValid();
        case 2:
          return ResultFactory.question4IsValid();
        case 3:
          return ResultFactory.question5IsValid();
        case 4:
          return ResultFactory.question6IsValid();
        case 5:
          return true;
        case 6:
          return true;
        default:
          return false;
      }
      }else{
      //console.log(Object.keys(ResultFactory));
      switch(number) {
        case 1:
          return ResultFactory.question1IsValid();
        case 2:
          return ResultFactory.question2IsValid();
        case 3:
          return ResultFactory.question3IsValid();
        case 4:
          return ResultFactory.question4IsValid();
        case 5:
          return ResultFactory.question5IsValid();
        case 6:
          return ResultFactory.question6IsValid();
        default:
          return false;
      }
    }
      /*
      var notEmpty = function (input) {
        return ResultFactory[input] !== null && ResultFactory[input] !== undefined;
      };

      return inputs[parseInt(number) - 1].every(notEmpty);*/
    };

    this.numberOfInputs = function () {
      return inputsFlatten.length;
    };

    this.numberOfValidInputs = function () {
      return inputsFlatten.filter(function(i) {
        return ResultFactory[i] !== null && ResultFactory[i] !== undefined;
      }).length;
    };

  });

