'use strict';

angular.module('ngApp')
  .controller('MenuController', function ($scope, $location, $localStorage,
                                          QuestionFactory, MenuService) {

    $scope.modalVisibility = 'hide';
    $scope.questions = QuestionFactory.getQuestions();

    if($localStorage.unit==='m2') {
      $scope.currentQuestion = $localStorage.currentQuestion-2;
    }else{
      $scope.currentQuestion = $localStorage.currentQuestion;
    }
    
    $scope.showModal = function() {
      $scope.modalVisibility = 'show';
    };

    $scope.openQuestion = function (number) {
      $location.url('/question/' + number);
    };

    $scope.isAnswered = function (number) {
      return MenuService.isAnswered(number);
    };

    $scope.questionSkipped = function (number) {
      return MenuService.questionSkipped(number);
    };

  });
