'use strict';

angular.module('resultFactoryModule')

  .factory('ResultFactory', function ($localStorage, VARIABLES) {
    var result = {

      propertyArea: undefined,
      regulatedToOther: undefined,
      builtResidence: undefined,
      builtGarage: undefined,
      builtSmallBuilding: undefined,
      builtOther: undefined,
      tearDownArea: undefined,
      newRegistredArea: undefined,
      parkingPlaces: undefined,
      parkingPlaceArea: undefined,
      parkingPlacesGarage: undefined,
      before2007Regulation: undefined,
      utilizationPercent: undefined,
      utilizationArea: undefined,
      hasRequiredExtraTerrainParkinPlaces: undefined,
      requiredExtraTerrainParkingPlaces : undefined,
      hasRequiredExtraTerrainParkingPlacesInRegPlan: undefined,


      loadQuestionsFromStorage: function () {
        this.propertyArea = $localStorage.propertyArea;
        this.regulatedToOther = $localStorage.regulatedToOther;
        this.builtResidence = $localStorage.builtResidence;
        this.builtGarage = $localStorage.builtGarage;
        this.builtSmallBuilding = $localStorage.builtSmallBuilding;
        this.builtOther = $localStorage.builtOther;
        this.tearDownArea = $localStorage.tearDownArea;
        this.newRegistredArea = $localStorage.newRegistredArea;
        this.parkingPlaces = $localStorage.parkingPlaces;
        this.parkingPlaceArea = $localStorage.parkingPlaceArea;
        this.parkingPlacesGarage = $localStorage.parkingPlacesGarage;
        this.before2007Regulation = $localStorage.before2007Regulation;
        this.utilizationPercent = $localStorage.utilizationPercent;
        this.utilizationArea = $localStorage.utilizationArea;
        this.requiredExtraTerrainParkingPlaces = $localStorage.requiredExtraTerrainParkingPlaces;
        this.hasRequiredExtraTerrainParkinPlaces = $localStorage.hasRequiredExtraTerrainParkinPlaces;
        this.hasRequiredExtraTerrainParkingPlacesInRegPlan = $localStorage.hasRequiredExtraTerrainParkingPlacesInRegPlan;
      },

      nullOrUndefined: function(prop) {
        if(typeof(prop) === 'undefined') {
          return true;
        }else if(prop === null) {
          return true;
        }
        return false;
      },

      setAndPersistProperty: function(key, value) {
        if (key in this && typeof this[key] !== 'function') {
          this[key] = value;
          if (this[key] === undefined || this[key] === null) {
            delete $localStorage[key];
          } else {
            $localStorage[key] = this[key];
          }
        }
      },

      hasUtilization: function () {
        return (this.utilizationArea !== undefined && this.utilizationArea !== null) ||
          (this.utilizationPercent !== undefined && this.utilizationPercent !== null);
      },

      isUtilizationPercent: function () {
        return this.utilizationPercent !== undefined;
      },

      setUtilizationArea: function (area) {
        this.setAndPersistProperty('utilizationArea', area);
        this.setAndPersistProperty('utilizationPercent', undefined);
      },

      setUtilizationPercent: function (percent) {
        this.setAndPersistProperty('utilizationArea', undefined);
        this.setAndPersistProperty('utilizationPercent', percent);
      },

      getUtilization: function () {
        return this.isUtilizationPercent() ? this.utilizationPercent : this.utilizationArea;
      },

      allowedArea: function () {
        if (this.isUtilizationPercent()) {
          return this.availableArea() * this.utilizationPercent / 100;
        } else {
          return this.utilizationArea;
        }
      },

      availableArea: function () {
        return this.propertyArea - this.regulatedToOther;
      },

      isValidResult: function () {
        return this.allowedArea() >= this.sumBuiltArea();
      },

      isExceedingAllowedArea: function () {
        return this.allowedArea() < this.sumBuiltArea();
      },

      numberOfProperties: function () {
        var totalProps = 0;
        for (var prop in this) {
          if (this.hasOwnProperty(prop) && typeof this[prop] !== 'function') {
            totalProps++;
          }
        }
        return totalProps;
      },

      isIncomplete: function () {
        var validationFunctions = [this.question1IsValid, this.question2IsValid, this.question3IsValid, this.question4IsValid, this.question5IsValid, this.question6IsValid];
        if($localStorage.unit==='m2') {
          validationFunctions = [this.question3IsValid, this.question4IsValid, this.question5IsValid, this.question6IsValid];
        }
        for(var i in validationFunctions) {

          if(!validationFunctions[i].call(this)) {
            return true;
          }
        }
        // Do a final check on utilization are or percent defined
        if(this.utilizationArea === undefined || this.utilizationArea===null) {
            if(this.utilizationPercent=== undefined || this.utilizationPercent===null) {
              return true;
          }
        }
        return false;
      },

      question1IsValid: function() {
        if(this.nullOrUndefined(this.propertyArea)) {
          return false;
        }else{
          return true;
        }
      },

      question2IsValid: function() {
        if(this.nullOrUndefined(this.regulatedToOther)) {
          return false;
        }else{
          return true;
        }
      },

      question3IsValid: function() {
        if(this.nullOrUndefined(this.builtResidence)) {
          return false;
        }else if(this.nullOrUndefined(this.builtGarage)) {
          return false;
        }else if(this.nullOrUndefined(this.builtSmallBuilding)) {
          return false;
        }else if(this.nullOrUndefined(this.builtOther)) {
          return false;
        }else{
          return true;
        }
      },

      question4IsValid: function() {
        if(this.nullOrUndefined(this.tearDownArea)) {
          return false;
        }else{
          return true;
        }
      },

      question5IsValid: function() {
        if(this.nullOrUndefined(this.newRegistredArea)) {
          return false;
        }else{
          return true;
        }
      },

      question6IsValid: function() {
        var calcMethod = $localStorage.calculationMethod;
        if(this.question6NewParkingIsValid.call(this)) {
          return true;
        }else{
          return false;
        }

      },

      question6NewParkingIsValid: function() {

        if(this.nullOrUndefined(this.parkingPlacesGarage) || this.nullOrUndefined(this.parkingPlaces)) {
              return false;
        }
        if(this.nullOrUndefined(this.hasRequiredExtraTerrainParkingPlacesInRegPlan)) {
              return false;
        } else {

          if(this.nullOrUndefined(this.requiredExtraTerrainParkingPlaces)) {
            if(this.hasRequiredExtraTerrainParkingPlacesInRegPlan === false) {
              //continue
            }else{
              return false;
            }
          }
        }

        var terrainParkingPlaces = this.parkingPlaces-this.parkingPlacesGarage;
        if(terrainParkingPlaces<=0) {
          return true;
        }else{
          if(this.nullOrUndefined(this.parkingPlaceArea)) {
            return false;
          }else{
            return true;
          }
        }
      },

      checkIfUtilizationAreaOrPercentIsEmpty : function() {
          return ((this.utilizationArea && this.utilizationPercent === undefined || this.utilizationPercent === null) ||
          (this.utilizationPercent && this.utilizationArea === undefined || this.utilizationArea === null));
      },

      isEmpty: function () {
        return this.numberOfEmptyProperties() === this.numberOfProperties();
      },

      emptyProperties: function () {
        var emptyProperties = [];
        for (var prop in this) {
          if (this.hasOwnProperty(prop) &&
            this[prop] === undefined ||
            this[prop] === null) {
            emptyProperties.push(prop);
          }
        }
        return emptyProperties;
      },

      numberOfEmptyProperties: function () {
        var emptyProperties = this.emptyProperties();
        return emptyProperties.length;
      },

      resultType: function () {
        if (this.isIncomplete()) {
          return 'incomplete';
        } else if (this.isValidResult()) {
          return 'valid';
        } else if (this.isExceedingAllowedArea()) {
          return 'exceeding';
        } else {
          console.log('Warning: ResultFactory resultType is unkown');
          return 'unkown';
        }
      },

      sumBuiltArea: function () {

          return (this.builtResidence + this.builtGarage + result.builtSmallBuilding + this.builtOther) -
            this.tearDownArea +
            this.newRegistredArea +
            this.sumParkingPlaceArea();
        
      },

      sumParkingPlaceArea: function () {
          var parkingPlacesTerrain = this.parkingPlaces-this.parkingPlacesGarage;
          
          if(this.hasRequiredExtraTerrainParkingPlacesInRegPlan) {
            if(this.requiredExtraTerrainParkingPlaces !== (undefined || null)) {
              if (this.requiredExtraTerrainParkingPlaces>parkingPlacesTerrain) {
                parkingPlacesTerrain = this.requiredExtraTerrainParkingPlaces;
              }
            }
          }

          if(parkingPlacesTerrain <1) {
            return 0;
          }else{
            return (parkingPlacesTerrain*this.parkingPlaceArea);
          }
      },

      

      reset: function () {
        for (var prop in this) {
          if (this.hasOwnProperty(prop) && typeof this[prop] !== 'function') {
            this[prop] = undefined;
            delete $localStorage[prop];
          }
        }
      }

    };

    return result;
  });
