'use strict';

angular.module('ngApp')
  .controller('ResultRowController', function ($scope, ResultFactory, $localStorage) {
    var result = ResultFactory;
    if($localStorage.unit === 'm2') {
      var questionResults = [
      result.builtResidence + result.builtGarage + result.builtSmallBuilding + result.builtOther,
      result.tearDownArea,
      result.newRegistredArea,
      result.sumParkingPlaceArea()
    ];
    }else{
      var questionResults = [
      result.propertyArea,
      result.regulatedToOther,
      result.builtResidence + result.builtGarage + result.builtSmallBuilding + result.builtOther,
      result.tearDownArea,
      result.newRegistredArea,
      result.sumParkingPlaceArea()
    ];
    }
    


    $scope.resultFor = function (number) {
      return questionResults[number -1];
    };

    $scope.resultForInput = function (key) {
      return ResultFactory[key];
    };

    $scope.sumBuiltArea = function() {
      return $scope.format(result.sumBuiltArea());
    };

    $scope.allowedArea = function() {
      return $scope.format(result.allowedArea());
    };

    $scope.builtExceedsAllowed = function() {
      return result.sumBuiltArea() > result.allowedArea();
    };
  });