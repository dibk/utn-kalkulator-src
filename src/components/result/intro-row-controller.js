'use strict';

angular.module('ngApp')
  .controller('IntroRowController', function ($scope, IntroFactory, VARIABLES) {

    var headers = {

      regulated: 'Er eiendommen regulert?',
      decision: 'Er det bestemmelser i kommuneplanen eller -delplanen som sier noe om tillatt utnyttelse?',
      regulationPlan: 'Står det noe i kommuneplanen eller delplanen om at den gjelder foran reguleringsplanen?',
      calculationMethod: 'Hvordan defineres grad av utnytting i ' + IntroFactory.regulationPlan + '?',
      regulationTime: 'Når er ' + IntroFactory.regulationPlan + ' vedtatt?' // TODO This isn't explicitly answered if user chooses BRA or %BRA. Confusing.

    };

    var years = {};
    years[VARIABLES.KEYS.m2BYAKey] = '1.7.2007';
    years[VARIABLES.KEYS.pctBYAKey] = '1.7.2007';
    years[VARIABLES.KEYS.m2TBRAKey] = '1.7.1997';
    years[VARIABLES.KEYS.pctTUKey] = '1.7.1997';
    years[VARIABLES.KEYS.m2BRAKey] = '1.7.1997';
    years[VARIABLES.KEYS.pctBRAKey] = '1.7.1997';

    var norwegian = {};
    norwegian['before'] = 'Før';
    norwegian['after'] = 'Etter';

    $scope.introQuestions = (function () {

      var questions = [];

      var regulated = {
        header: headers.regulated,
        answer: IntroFactory.regulated ? 'Ja' : 'Nei'
      };
      questions.push(regulated);

      if (typeof IntroFactory.decision !== 'undefined') {
        var decision = {
          header: headers.decision,
          answer: IntroFactory.decision ? 'Ja' : 'Nei'
        };
        questions.push(decision);
      }

      if (IntroFactory.regulated) {
        var regulationPlan = {
          header: headers.regulationPlan,
          answer: IntroFactory.regulationPlan === 'reguleringsplanen' ? 'Nei' : 'Ja'
        };
        questions.push(regulationPlan);
      }

      var calculationMethod = {
        header: headers.calculationMethod,
        answer: IntroFactory.calculationMethod
      };
      questions.push(calculationMethod);

      if (IntroFactory.calculationMethod !== VARIABLES.KEYS.m2BRAKey &&
        IntroFactory.calculationMethod !== VARIABLES.KEYS.pctBRAKey) {
        var regulationTime = {
          header: headers.regulationTime,
          answer: norwegian[IntroFactory.regulationTime] + ' ' +  years[IntroFactory.calculationMethod]
        };
        questions.push(regulationTime);
      }

      return questions;
    })();

  });
