'use strict';

angular.module('ngApp')
  .controller('ResultController', function ($scope, $window, $location, $localStorage,
                                            MenuService, ResultFactory, QuestionFactory) {

    $scope.questions = QuestionFactory.getQuestions();

    $scope.currentQuestion = $localStorage.currentQuestion;
    if(!ResultFactory.isUtilizationPercent()) {
      $scope.currentQuestion -=2;
    }

    $scope.resultType = (function () {
      return ResultFactory.resultType();
    })();

    $scope.showPrintButton = typeof $window.print === 'function';

    $scope.openPrintWindow = function () {
      $window.print();
    };

    $scope.showModal = function () {
      $scope.modalVisibility = 'show';
    };

    $scope.goTo = function (number) {
      $location.url('/question/' + number);
    };

    $scope.isAnswered = function (number) {
      return MenuService.isAnswered(number);
    };

    $scope.format = function (number) {
      return parseFloat(number).toFixed(2);
    };

    $scope.removeMinus = function (number) {
      return Math.abs(number).toFixed(2);
    };

    $window.scrollTo(0, 0);

  });
