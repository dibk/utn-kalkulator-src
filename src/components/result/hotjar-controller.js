'use strict';

angular.module('ngApp')
  .controller('HotjarController', function (angularLoad) {
    angularLoad.loadScript('/components/result/hotjar.js');
  });