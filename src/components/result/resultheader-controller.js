'use strict';

angular.module('ngApp')
  .controller('ResultHeaderController', function ($scope, ResultFactory) {
    var result = ResultFactory;

    $scope.demand = $scope.format(result.getUtilization());

    $scope.unit = result.isUtilizationPercent() ? '%' : 'm2';

    $scope.isUtilizationPercent = result.isUtilizationPercent();

    $scope.usedAreaPercent = $scope.format(result.sumBuiltArea() / result.availableArea() * 100);

    $scope.sumBuiltArea = $scope.format(result.sumBuiltArea());

    $scope.allowedArea = $scope.format(result.allowedArea());
  });
