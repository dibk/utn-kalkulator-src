'use strict';

angular.module('ngApp')
  .controller('ProgressbarController', function ($scope, MenuService) {

    $scope.progress = function() {
      return (MenuService.numberOfValidInputs() / MenuService.numberOfInputs()) * 100;
    };

  });