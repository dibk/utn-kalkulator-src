'use strict';

angular.module('directives')
  .directive('confirmIntroChangeModal', function () {
    return {
      restrict: 'E',
      templateUrl: 'components/directives/confirm-intro-change-modal.html',
      link: function ($scope) {
        $scope.restart = function() {
          $scope.confirmChangeModal();
          $scope.confirmModalVisibility = 'hide';
        };
        $scope.cancel = function() {
          $scope.confirmModalVisibility = 'hide';
          $scope.cancelConfirmChangeModal();
          //$scope.$digest();
        };
      }
    };
  });
