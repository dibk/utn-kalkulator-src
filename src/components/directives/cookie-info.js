'use strict';

angular.module('directives')
  .directive('cookieInfo', function (ipCookie) {

    return {
      restrict: 'AE',
      templateUrl: '/components/cookie-info/cookie-bar.html',
      scope: true,
      link: function ($scope) {
        var COOKIE_KEY = 'utn-accepted-cookies-notice';
        $scope.showCookieInfo = ipCookie(COOKIE_KEY) ? false : true;
        $scope.closeCookieInfo = function () {
          $scope.showCookieInfo = false;
          ipCookie(COOKIE_KEY, true, {
            expires: 365
          });
        };
      }
    };
  });