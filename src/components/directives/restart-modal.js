'use strict';

angular.module('directives')
  .directive('restartModal', function ($localStorage, $location, ResultFactory, IntroFactory) {
    return {
      restrict: 'E',
      templateUrl: 'components/directives/restart-modal.html',
      link: function ($scope) {
        $scope.restart = function() {
          $localStorage.$reset();
          ResultFactory.reset();
          IntroFactory.reset();
          $location.path('/');
        };
        $scope.cancel = function() {
          $scope.modalVisibility = 'hide';
        };
      }
    };
  });