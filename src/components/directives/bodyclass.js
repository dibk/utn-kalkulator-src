/* jshint browser: true */
'use strict';

angular.module('directives')
  .directive('bodyClass', function () {

    function link(scope, element, attrs) {
      var body = angular.element(document).find('body');
      var removeClass = function () {
        body.removeClass(attrs.bodyClass);
      };

      body.addClass(attrs.bodyClass);
      scope.$on('$destroy', removeClass);
    }

    return {
      restrict: 'A',
      link: link
    };
  });
