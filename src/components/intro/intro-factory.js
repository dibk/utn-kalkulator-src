'use strict';

angular.module('introFactoryModule')
  .factory('IntroFactory', function ($localStorage) {
    return {

      showOverridingQuestion: false,
      showError: false,
      showErrorNoCalcMethod: false,
      regulated: undefined,
      decision: undefined,
      regulationTime: undefined,
      regulationPlan: undefined,
      unit: undefined,
      utilizationArea: undefined,
      calculationMethod: undefined,
      showUtilizationInput: undefined,

      reset: function () {
        this.showOverridingQuestion = false;
        this.showError = false;
        this.showErrorNoCalcMethod = false;
        this.regulated = undefined;
        this.decision = undefined;
        this.regulationTime = undefined;
        this.regulationPlan = undefined;
        this.unit = undefined;
        this.utilizationArea = undefined;
        this.calculationMethod = undefined;
        this.showUtilizationInput = undefined;
      },

      loadIntroFromStorage: function () {
        this.showOverridingQuestion = $localStorage.showOverridingQuestion || false;
        this.showError = $localStorage.showError || false;
        this.showErrorNoCalcMethod = $localStorage.showErrorNoCalcMethod || false;
        this.regulated = $localStorage.regulated;
        this.decision = $localStorage.decision;
        this.regulationTime = $localStorage.regulationTime;
        this.regulationPlan = $localStorage.regulationPlan;
        this.unit = $localStorage.unit;
        this.utilizationArea = $localStorage.utilizationArea;
        this.calculationMethod = $localStorage.calculationMethod;
        this.showUtilizationInput = $localStorage.showUtilizationInput;
      },

      persistProperty: function(key) {
        if (key in this && typeof this[key] !== 'function') {
          if (this[key] === undefined || this[key] === null) {
            delete $localStorage[key];
          } else {
            $localStorage[key] = this[key];
          }
        }
      },

      setAndPersistProperty: function(key, value) {
        if (key in this && typeof this[key] !== 'function') {
          this[key] = value;
          this.persistProperty(key);
        }
      },

    };
  });
