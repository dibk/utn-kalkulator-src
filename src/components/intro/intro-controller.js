'use strict';

angular.module('ngApp')
  .controller('IntroController', function ($scope, $location, $localStorage, ResultFactory, IntroFactory, MenuService, VARIABLES) {

    var revertProperty = {};

    $scope.area = VARIABLES.KEYS.m2Key;
    $scope.pct = VARIABLES.KEYS.pctKey;

    $scope.m2BYA = VARIABLES.KEYS.m2BYAKey;
    $scope.pctBYA = VARIABLES.KEYS.pctBYAKey;
    $scope.m2TBRA = VARIABLES.KEYS.m2TBRAKey;
    $scope.pctTU = VARIABLES.KEYS.pctTUKey;
    $scope.m2BRA = VARIABLES.KEYS.m2BRAKey;
    $scope.pctBRA = VARIABLES.KEYS.pctBRAKey;
    $scope.parkingNormTerm = VARIABLES.KEYS.parkingNormKey;

    $scope.years = {};
    $scope.years[$scope.m2BYA] =  '1.7.2007';
    $scope.years[$scope.pctBYA] = '1.7.2007';
    $scope.years[$scope.m2TBRA] = '1.7.1997';
    $scope.years[$scope.pctTU] =  '1.7.1997';
    $scope.years[$scope.m2BRA] =  '1.7.1997';
    $scope.years[$scope.pctBRA] = '1.7.1997';

    // Boolean value indicating if user is filling out the intro-part
    // with or without having answered any questions from the questions-part
    var dirtyRun = (function () {
      var utilizationIsSetOnce = ResultFactory.hasUtilization() &&
        (!ResultFactory.utilizationArea || !ResultFactory.utilizationPercent);

      return ((MenuService.numberOfValidInputs() > 0) && utilizationIsSetOnce);
    }());

    //$scope.showUtilizationInput = typeof IntroFactory.utilizationArea == 'undefined' ?  false : true;

    var decisionWasFalsyfied = function () {
      IntroFactory.setAndPersistProperty('regulationTime', undefined);
      IntroFactory.setAndPersistProperty('regulationPlan', undefined);
      IntroFactory.setAndPersistProperty('unit', undefined);
      IntroFactory.setAndPersistProperty('utilizationArea', undefined);
      ResultFactory.setAndPersistProperty('utilizationPercent', undefined);
      ResultFactory.setAndPersistProperty('utilizationArea', undefined);
    };

    var setRegulationTime = function () {
      var value = IntroFactory.regulationTime;
      if (value === 'before') {
        ResultFactory.setAndPersistProperty('before2007Regulation', true);
      } else if (value === 'after') {
        ResultFactory.setAndPersistProperty('before2007Regulation', false);
      }
    };

    $scope.handlePatternUtilizationArea = (function() {
      var pctRegex = /^[0-9]{1,3}(\.[0-9]{1,2})?$/;
      var m2Regex = /^[0-9]+(\.[0-9]{1,2})?$/;
      return {
        test: function(value) {
          return IntroFactory.unit === '%' ? pctRegex.test(value) : m2Regex.test(value)
        }
      };
    }());

    $scope.IntroFactory = IntroFactory;

    IntroFactory.showUtilizationInput = typeof IntroFactory.utilizationArea == 'undefined' ?  false : true;

    $scope.confirmModalVisibility = 'hide';

    $scope.updateUtilization = function (unit) {
      if ($scope.IntroFactory.unit === 'm2') {
        ResultFactory.setUtilizationArea(unit);
      } else {
        ResultFactory.setUtilizationPercent(unit);
      }
    };

    // Cancel changes
    $scope.cancelConfirmChangeModal = function () {

      revertChanges();
      $scope.checkProceeding();

    };

    // Apply changes
    $scope.confirmChangeModal = function () {

      if (revertProperty.hasOwnProperty('decision') && IntroFactory.decision === false) {
        decisionWasFalsyfied();
      }

      MenuService.clearSkippedQuestions();
      $localStorage.currentQuestion = undefined;

      // Clear every answer from the question-part
      ResultFactory.reset();

      // If user confirmed calculationMethod-change
      // we'll clear regulationTime and utilizationArea, but only
      // if user has changed to some
      if (revertProperty.hasOwnProperty('calculationMethod')) {

        if (IntroFactory.calculationMethod !== VARIABLES.KEYS.m2BRAKey &&
            IntroFactory.calculationMethod !== VARIABLES.KEYS.pctBRAKey) {

              IntroFactory.regulationTime = undefined;

        }

        IntroFactory.utilizationArea = undefined;
        $scope.updateUtilization(undefined);


      }

      // If user confirmed utilizationArea-change
      // we'll need to update utilization in ResultFactory
      if (revertProperty.hasOwnProperty('utilizationArea')) {
        $scope.updateUtilization(IntroFactory.utilizationArea);
      }

      persistChanges();

      // User has confirmed clearing the wizard. Let's make
      // this a fresh run again
      dirtyRun = false;

    };

    /**
     * The two properties utilizationArea and utilizationPercent is set from intro,
     * thus is not part of the "question-part" of the app.
     * So we consider ResultFactory to be empty even when one of those is set (because
     * the user hasn't filled anything in the question-part).
     *
     * ng-change can use this to persist properties and to check
     * if result-factory should call reset()
     * @param key
     * @param oldVal
     *  String-values, if not we get compilation-error from ng-change.
     *  If undefined we assume it's always ok to change this value (e.g.: utilizationAreaForm)
     */
    $scope.introPropertyUpdated = function (key, oldVal) {

      persistProp(key, oldVal);
      if (key === 'decision' && IntroFactory.decision === false) {
        decisionWasFalsyfied();
      }
      if (key === 'decision' && IntroFactory.decision === true) {
        var oldValRegulationPlan = IntroFactory.regulationPlan;
        IntroFactory.regulationPlan = 'kommuneplanen';
        persistProp('regulationPlan', oldValRegulationPlan);
      }

      if (dirtyRun) { $scope.confirmModalVisibility = 'show'; }

    };

    $scope.calculationMethodUpdated = function (key, oldVal) {

      if (IntroFactory.calculationMethod == "none") {

        IntroFactory.showErrorNoCalcMethod = true;

        // Persist calculationMethod
        persistProp(key, oldVal);

        // Get and persist dependent property 'unit'
        var oldValUnit = IntroFactory.unit;
        IntroFactory.unit = $scope.getUnitFromCalculationMethod(IntroFactory.calculationMethod);
        persistProp('unit', oldValUnit);

        // Persist showErrorNoCalcMethod
        persistProp('showErrorNoCalcMethod', false);

        if (dirtyRun) { $scope.confirmModalVisibility = 'show'; }

        return;

      }

      // We know user has selected an actual calculationMethod (!none).
      // So, hide the error-message.
      IntroFactory.showErrorNoCalcMethod = false;

      // Persist properties in $LocalStorage or (if dirtyRun) save
      // them in the revertProperty-object.

      // Persist calculationMethod
      persistProp(key, oldVal);

      // Get and persist dependent property 'unit'
      var oldValUnit = IntroFactory.unit;
      IntroFactory.unit = $scope.getUnitFromCalculationMethod(IntroFactory.calculationMethod);
      persistProp('unit', oldValUnit);

      var oldValShowUtilizationInput = IntroFactory.showUtilizationInput;
      if (IntroFactory.calculationMethod === VARIABLES.KEYS.m2BRAKey ||
          IntroFactory.calculationMethod === VARIABLES.KEYS.pctBRAKey) {

        IntroFactory.showUtilizationInput = true;

        var oldValRegulationTime = IntroFactory.regulationTime;
        IntroFactory.regulationTime = 'after';
        persistProp('regulationTime', oldValRegulationTime);

      } else {
        IntroFactory.showUtilizationInput = false;
      }
      persistProp('showUtilizationInput', oldValShowUtilizationInput);

      if (dirtyRun) { $scope.confirmModalVisibility = 'show'; }

      // If user changes calculationMethod, chances are that the entered utilization
      // is not correct anymore (eg. going from m2 to %), so we'd want to clear the value.
      // This is ofcourse only necessary if we actually have an entered utilizationArea value,
      // and we are looking at a fresh run (no questions after the intro has been answered).
      // The same applies to regulationTime
      if (typeof(IntroFactory.regulationTime != 'undefined') && !dirtyRun) {
          IntroFactory.regulationTime = undefined;
      }

      if (typeof(IntroFactory.utilizationArea) != 'undefined' && !dirtyRun) {
        IntroFactory.utilizationArea = undefined;
        $scope.updateUtilization(undefined);
      }

    };

    var persistProp = function(key, oldVal) {
      if (dirtyRun) {
        revertProperty[key] = oldVal;
      } else {
        IntroFactory.persistProperty(key);
      }
    };

    var revertChanges = function() {
      for (var k in revertProperty) {
        if (revertProperty.hasOwnProperty(k)) {
          if (revertProperty[k] === 'true') {
            revertProperty[k] = true;
          } else if (revertProperty[k] === 'false') {
            revertProperty[k] = false;
          }
          IntroFactory.setAndPersistProperty(k, revertProperty[k]);
        }
      }
      revertProperty = {};
    }

    var persistChanges = function() {
      for (var k in revertProperty) {
        if (revertProperty.hasOwnProperty(k)) {
          IntroFactory.persistProperty(k);
        }
      }
      revertProperty = {};
    }

    $scope.checkProceeding = function () {

      var regulated = IntroFactory.regulated;
      var decision = IntroFactory.decision;

      if (typeof regulated === 'undefined') {
        return;
      }

      if (regulated) {
        IntroFactory.showError = false;
        IntroFactory.showOverridingQuestion = true;
      } else if (regulated === false && typeof decision === 'undefined') {
        IntroFactory.showError = false;
        IntroFactory.showOverridingQuestion = false;
      } else if (regulated === false && decision === false) {
        IntroFactory.showError = true;
        IntroFactory.showOverridingQuestion = false;
      } else {
        IntroFactory.showError = false;
        IntroFactory.showOverridingQuestion = true;
      }

      IntroFactory.persistProperty('showError');
      IntroFactory.persistProperty('showOverridingQuestion');

    };

    $scope.preventProceeding = function() {
      return typeof IntroFactory.calculationMethod === "undefined" || !ResultFactory.hasUtilization() || IntroFactory.showErrorNoCalcMethod;
    };

    $scope.proceed = function () {
      if (!$scope.preventProceeding()) {
        setRegulationTime();
        $location.url('/questions');
      }
    };

    $scope.getUnitFromCalculationMethod = function(calcMethod) {
      if (typeof calcMethod !== 'undefined') {
        return calcMethod.charAt(0) == '%' ? $scope.pct : $scope.area;
      }
    };

    MenuService.removeAllSkippedQuestions();

  });
