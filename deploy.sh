#!/usr/bin/env bash

function show_help {
  echo -e "Usage:\n"
  echo "./deploy.sh  [-v | -h]"
  echo -e "./deploy.sh -e <environment>\n"
  echo -e "Options:\n"
  echo -e "\t -h"
  echo -e "\t\t Print this help.\n"
  echo -e "\t -v"
  echo -e "\t\t Tag a new version. User enters a new version, which will be then tagged and pushed to git."
  echo -e "\t\t Config-file containing current version will be modified during process.\n"
  echo -e "\t -n"
  echo -e "\t\t Advance to a new version. No tagging or pushing will occur."
  echo -e "\t\t Config-file containing current version will be modified during process.\n"
  echo -e "\t -e <staging|prod>\n"
  echo -e "\t\t Build and deploy current version to the given environment. It is expeced that user has"
  echo -e "\t\t set environment variables \$STAGING_REPO and \$PROD_REPO, which point to directories at"
  echo -e "\t\t local disk where artifacts will be copied, and where from artifacts will be pushed to the"
  echo -e "\t\t given environment.\n"
  echo -e "Examples:\n"
  echo -e "\t ./deploy.sh -e staging"
  echo -e "\t\t Build and deploy current code to staging-environment.\n"
  echo -e "\t ./deploy.sh -v -e staging"
  echo -e "\t\t Tag new version to git, build from source and deploy new version to staging-environment.\n"
  echo -e "\t ./deploy.sh -n -e staging"
  echo -e "\t\t Tag new version to git, build from source and deploy new version to staging-environment."
  echo -e "\t\t Advance development version (not commit it to git).\n"
}

function show_repo_warning {
  echo "You have to set environment variabel to STAGING|PROD_REPO to point"
  echo "to your deployment repository at local disk."
}

function init_environment {
  printf "Setting deployment target to : $1\n\n"

  case $1 in
    staging)
      [ -z "$STAGING_REPO" ] && show_repo_warning staging && exit 1;
      ;;
    prod)
      [ -z "$PROD_REPO" ] && show_repo_warning prod && exit 1;
      ;;
  esac
}

function build {
  echo "Building project to $1 ...."
  gulp build-$1
  clear
}

function copy_build_to {
  dest_dir=$(echo $1 | awk '{print toupper($0)}')_REPO
  echo "Copying everything under dist-folder to ${!dest_dir}"
  cp -r dist/* ${!dest_dir}
  echo "Done copying..."
}

function push_to {
  dest_dir=$(echo $1 | awk '{print toupper($0)}')_REPO
  echo "Change dir to ${dest_dir}"
  cd ${!dest_dir}

  echo "Current dir is now ${PWD}"
  sleep 3
  echo -e "Checking git status after pulling:\n\n"
  git pull
  git status

  echo -e "Adding everything to git index"
  sleep 2
  git add .
  git status

  echo -e "Committing to git"
  read -p "Enter commit message: " msg
  git commit -a -m "${msg:-$CI_MSG at $(date)}"

  echo -e "pushing changes to repository ${dest_dir}"
  sleep 2
  git push
}

function modify_config {
  echo "Setting version to: $3"
  sed -i.bak s/${2}/${3}/ $1
  rm ${1}.bak
}

function tag_version {
  config_file=src/config.json
  current_version=$(grep -m 1 version ${config_file} | cut -d \" -f 4)

  echo "Current version is now ${current_version}"
  read -p "Enter new version: " new_version
  modify_config ${config_file} ${current_version} ${new_version}

  git commit -a -m "version $new_version"
  git tag -a version-${new_version} -m "version $new_version tagged"
  git push
  git push --tags
}

function new_version {
  config_file=src/config.json
  current_version=$(grep -m 1 version ${config_file} | cut -d \" -f 4)
  echo "Current version is now ${current_version}"

  read -p "Enter new development version: " new_dev_version
  modify_config ${config_file} ${current_version} ${new_dev_version}
  git commit -a -m "version changed to $new_dev_version"
}

while getopts "vhne:" opt; do
  case "$opt" in
    e)
      if [ $OPTARG != "staging" -a $OPTARG != "prod" ]; then
        echo "Only staging and prod environments are available"
      else
        init_environment $OPTARG
        build $OPTARG
        copy_build_to $OPTARG
        push_to $OPTARG
      fi
      ;;
    v)
      tag_version
      ;;
    n)
      new_version
      ;;
    h)
      show_help
      exit 0
      ;;
    *)
      echo "Invalid option try -h for help."
      ;;
  esac
done

