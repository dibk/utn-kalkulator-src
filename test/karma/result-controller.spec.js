'use strict';

describe('Result controller', function () {
  var scope = {};

  beforeEach(module('ngApp'));
  beforeEach(inject(function($controller) {
    $controller('ResultController', {$scope: scope});
  }));

  it('should format number to 2 decimals', function() {
    expect(scope.format(22.8345722)).toBe('22.83');
    expect(scope.format(2.456)).toBe('2.46');
    expect(scope.format(10)).toBe('10.00');
  });

  it('should remove minus sign on negative values and contain 2 decimals', function() {
    expect(scope.removeMinus(-220)).toBe('220.00');
    expect(scope.removeMinus(-3.2323)).toBe('3.23');
    expect(scope.removeMinus(34.222)).toBe('34.22');
  });

});
