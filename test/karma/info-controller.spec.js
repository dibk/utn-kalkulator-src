'use strict';

describe('Info controller', function () {
  var scope = {};
  var localStorage = {regulationTime: 'before'};
  beforeEach(module('ngApp'));
  beforeEach(inject(function ($controller) {
    $controller('InfoController', {$scope: scope, $routeParams: {number: 10}, $localStorage: localStorage})
  }));

  it('should set question number from routeparams', function () {
    expect(scope.number).toBe(10);
  });

  it('should set regulation time from localstorage', function () {
    expect(scope.regulationTime).toBe('before');
  });

});
