'use strict';

describe('Confirm intro change modal', function () {

  var element, scope, confirmChange, cancelConfirmChange;

  beforeEach(module('directives'));
  beforeEach(module('gulpAngular'));

  beforeEach(inject(function($rootScope, $compile) {
    scope = $rootScope.$new();
    scope.confirmChangeModal = function() {
      confirmChange = true;
    };
    scope.cancelConfirmChangeModal = function () {
      cancelConfirmChange = true;
    };
    element = $compile('<confirm-intro-change-modal></confirm-intro-change-modal>')(scope);
    scope.$digest();
  }));

  it('should have restart and cancel functions defined in scope', function () {
    expect(element.scope().restart).toBeDefined();
    expect(element.scope().cancel).toBeDefined();
  });

  it('should hide the modal on cancel', function () {
    element.scope().cancel();
    expect(scope.confirmModalVisibility).toEqual('hide');
  });

  it('should hide the modal on restart', function () {
    element.scope().restart();
    expect(scope.confirmModalVisibility).toEqual('hide');
  });

  it('should tell super-scope to confirm on confirm', function () {
    element.scope().cancel();
    expect(cancelConfirmChange).toBeTruthy();
  });

  it('should tell super-scope to cancel on cancel', function () {
    element.scope().restart();
    expect(confirmChange).toBeTruthy();
  });

});