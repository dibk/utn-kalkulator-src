'use strict';

describe('Info controller', function () {
  var scope = {};
  var location = {};
  var MenuService =  {};
  var localStorage = {currentQuestion: 10};
  var questions = {
      getQuestions : function() { return 1}
  };


  beforeEach(module('ngApp', function($provide) {
    //$provide.value('QuestionFactory', questions);
  }));

  beforeEach(inject(function($controller) {
    $controller('MenuController', {$scope: scope, $location : location, $localStorage: localStorage, QuestionFactory: questions, MenuService : MenuService})
  }));

  it('should set questions from constants', function() {
    expect(scope.questions).toBe(questions.getQuestions());
  });

  it('should have modal hidden by default', function() {
    expect(scope.modalVisibility).toBe('hide');
  });

  it('should set modal visible when showModal is called', function() {
    expect(scope.modalVisibility).toBe('hide');
    scope.showModal();

    expect(scope.modalVisibility).toBe('show');
  });

  it('should set current question from localstorage', function() {
    expect(scope.currentQuestion).toBe(10);
  });

});
