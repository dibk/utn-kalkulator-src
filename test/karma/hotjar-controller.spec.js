'use strict';

describe('Hotjar controller', function () {
  var angularLoad;

  beforeEach(module('ngApp'));
  beforeEach(inject(function($controller, _angularLoad_) {
    angularLoad = _angularLoad_;
    spyOn(angularLoad, 'loadScript');
    $controller('HotjarController', {angularLoad: angularLoad})
  }));

  it('should load hotjar script', function() {
    expect(angularLoad.loadScript).toHaveBeenCalledWith('/components/result/hotjar.js');
  });

});
