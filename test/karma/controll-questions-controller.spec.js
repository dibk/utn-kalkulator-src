'use strict';

describe('ControlQuestionsController', function () {
  var scope = {};
  var qFactory = {
    getBaseCalculationMethod: function() {return "BRA";},
    getDescription: function() {}

  }
  var localStorage = {currentQuestion: 3, calculationMethod: "BRA"};

  beforeEach(module('ngApp'));
  beforeEach(inject(function($controller) {
    spyOn(qFactory, 'getBaseCalculationMethod').and.returnValue("BRA");
    $controller('ControlQuestionsController', {$scope: scope, $localStorage: localStorage, QuestionFactory: qFactory})
  }));

  // it('was destroying my workflow').removeIf(isUnnecessary)
  // it('should contain 4 questions', function() {
  //   expect(scope.controlQuestions.length).toBe(4);
  // });

  it('should set current question from scope', function() {
    expect(scope.number).toBe(3);
  });

});
