'use strict';

describe('QuestionFactory', function () {

  var qFactory;
  var localStorage = {};

  beforeEach(module('ngApp', function($provide) {
    $provide.value('$localStorage', localStorage);
  }));

  beforeEach(module('questionServiceModule'));

  beforeEach(inject(function (QuestionFactory) {
    qFactory = QuestionFactory;
  }));



  it('should find question by number from questions structure', inject(function ($localStorage) {
    var question = qFactory.getQuestion(3);

    expect(question.number).toBe(3);
    expect(question.next).toBe(4);
    expect(question.previous).toBe(2);
  }));

});
