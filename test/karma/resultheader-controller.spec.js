'use strict';

describe('Resultheader controller', function () {
  var scope = {};
  var resultFactory;

  beforeEach(module('ngApp'));
  beforeEach(inject(function($controller, $rootScope, ResultFactory) {
    resultFactory = ResultFactory;
    spyOn(resultFactory, 'sumBuiltArea').and.returnValue(125.5678);
    spyOn(resultFactory, 'allowedArea').and.returnValue(250.2345);
    spyOn(resultFactory, 'availableArea').and.returnValue(550);
    spyOn(resultFactory, 'getUtilization').and.returnValue(1255);
    $controller('ResultController', {$scope: scope, $ResultFactory: ResultFactory});
    $controller('ResultHeaderController', {$scope: scope, $ResultFactory: ResultFactory})
  }));

  it('should have unit according to chosen utilization', function() {
    expect(resultFactory.isUtilizationPercent()).toBe(false);
    expect(scope.unit).toBe('m2')
  });

  it('should show correctly formatted usedAreaPercent', function() {
    expect(scope.usedAreaPercent).toBe('22.83');
  });

  it('should show correctly formatted built area', function() {
    expect(scope.sumBuiltArea).toBe('125.57');
  });

  it('should show correctly formatted allowed area', function() {
    expect(scope.allowedArea).toBe('250.23');
  });

  it('should show correctly formatted demand area', function() {
    expect(scope.demand).toBe('1255.00');
  });

});
