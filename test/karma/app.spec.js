'use strict';

describe('Module ngApp', function () {
  var  ngApp;

  beforeAll(function () {
    ngApp = module('ngApp');
  });

  it('should be registered', function () {
    expect(ngApp).not.toEqual(null);
  });

  it('along with other modules', function () {
    expect(module('constants')).not.toEqual(null);
    expect(module('config')).not.toEqual(null);
    expect(module('directives')).not.toEqual(null);
    expect(module('questionServiceModule')).not.toEqual(null);
    expect(module('navigationServiceModule')).not.toEqual(null);
    expect(module('menuServiceModule')).not.toEqual(null);
    expect(module('angularLoad')).not.toEqual(null);
    expect(module('angular-google-analytics')).not.toEqual(null);
  });

});
