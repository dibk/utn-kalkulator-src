'use strict';

describe('Restart modal directive', function () {

  var element, scope, location, localStorage, resultFactory, introFactory;

  beforeEach(module('directives'));
  beforeEach(module('gulpAngular'));
  beforeEach(module('resultFactoryModule'));
  beforeEach(module('introFactoryModule'));
  beforeEach(module('constants'));

  beforeEach(inject(function ($rootScope, $compile, $location, $localStorage, ResultFactory, IntroFactory) {
    introFactory= IntroFactory;
    resultFactory = ResultFactory;
    location = $location;
    localStorage = $localStorage;
    spyOn(introFactory, 'reset').and.callThrough();
    spyOn(location, 'path').and.callThrough();
    spyOn(localStorage, '$reset').and.callThrough();
    scope = $rootScope.$new();
    element = $compile('<restart-modal></restart-modal>')(scope);
    scope.$digest();
  }));

  it('should have restart and cancel functions defined in scope', function () {
    expect(element.scope().restart).toBeDefined();
    expect(element.scope().cancel).toBeDefined();
  });

  it('should reset localstorage and navigate to context root when restarting', function () {
    element.scope().restart();

    expect(location.path).toHaveBeenCalledWith('/');
    expect(localStorage.$reset).toHaveBeenCalled();
    expect(localStorage.currentQuestion).toBeUndefined();
  });

  it('should set modal hidden when canceled', function() {
    element.scope().cancel();
    expect(scope.modalVisibility).toEqual('hide');
  });

  it('should reset the ResultFactory on restart', function () {
    resultFactory['propertyArea'] = 20;
    resultFactory['before2007Regulation'] = true;

    element.scope().restart();

    expect(resultFactory['propertyArea']).toBeUndefined();
    expect(resultFactory['before2007Regulation']).toBeFalsy();
  });

  it('should reset the IntroFactory on restart', function () {
    introFactory['regulationPlan'] = 'kommuneplanen';

    element.scope().restart();

    expect(introFactory.reset).toHaveBeenCalled();
    expect(introFactory['regulationPlan']).toBeUndefined();
  });

});