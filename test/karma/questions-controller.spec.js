'use strict';

describe('Info controller', function () {
  var scope = {};
  var localStorage = {};
  var window;

  beforeEach(module('ngApp'));
  beforeEach(inject(function($rootScope, $controller, $window) {
    window = $window;
    spyOn(window, 'scrollTo');
    $controller('QuestionsController',
      {$scope: scope, $window: window, $routeParams: {number: 5}, $localStorage: localStorage})
  }));

  it('should set question number from routeparams', function() {
    expect(scope.number).toBe(5);
  });

  it('should set save current question to localstorage', function() {
    expect(localStorage.currentQuestion).toBe(5);
  });

  it('should scroll to 0.0', function () {
    expect(window.scrollTo).toHaveBeenCalledWith(0, 0);
  });

  it('should have resolve next and previous functions', function () {
    expect(scope.resolveNext).toBeDefined();
    expect(scope.resolvePrevious).toBeDefined();
  });

});
