'use strict';

describe('BookmarkService', function () {

  var bookmarkService, location, storage;

  beforeEach(function () {
    module('bookmarkServiceModule', function($provide) {

      location = {
        _url: 'someUrl',
        url: function(url) {
          if (url) {
            this._url = url;
          }
          return this._url;
        }
      };

      storage = {};

      $provide.value('$localStorage', storage);
      $provide.value('$location', location);

    });
  });

  beforeEach(inject(function(BookmarkService) {
    bookmarkService = BookmarkService;
  }));

  it('should be able to set bookmark', function () {
    expect(storage[bookmarkService.key]).toBeUndefined();
    bookmarkService.setBookmark();
    expect(storage[bookmarkService.key]).toBeDefined();
  });

  it('should be able to open bookmark', function () {
    var url = 'theBookmark';
    storage[bookmarkService.key] = url;
    bookmarkService.openBookmark();
    expect(location.url()).toEqual(url);
  });

});