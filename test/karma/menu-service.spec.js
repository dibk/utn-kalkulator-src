'use strict';

describe('MenuService', function () {
  var resultFactory = {
    propertyArea: undefined,
    builtResidence: undefined,
    builtGarage: undefined,
    builtSmallBuilding: undefined,
    builtOther: undefined,
    question1IsValid : function(){},
    question2IsValid : function(){},
    question3IsValid : function(){},
    question4IsValid : function(){},
    question5IsValid : function(){},
    question6IsValid : function(){}
  };
  var menuService;

  beforeEach(module('menuServiceModule', function ($provide) {
    $provide.value('ResultFactory', resultFactory);
  }));

  beforeEach(inject(function (MenuService) {
    menuService = MenuService;
  }));


  it('should have question not answered when field is null or undefined', function () {
    spyOn(resultFactory, 'question1IsValid').and.returnValue(false);
    expect(menuService.isAnswered(1)).toBe(false);
    expect(resultFactory.question1IsValid).toHaveBeenCalled();
    resultFactory.propertyArea = null;
    expect(menuService.isAnswered(1)).toBe(false);
  });

  it('should have question answered when field has value', function () {
    spyOn(resultFactory, 'question1IsValid').and.returnValue(true);
    resultFactory.propertyArea = 1;
    expect(menuService.isAnswered(1)).toBe(true);
    expect(resultFactory.question1IsValid).toHaveBeenCalled();
  });

  it('should have question answered when all fields have value', function () {
    spyOn(resultFactory, 'question3IsValid').and.returnValue(true);

    resultFactory.builtResidence = 1;
    resultFactory.builtGarage = 2;
    resultFactory.builtSmallBuilding = 3;
    resultFactory.builtOther = 5;

    expect(menuService.isAnswered(3)).toBe(true);
    expect(resultFactory.question3IsValid).toHaveBeenCalled();
  });

  it('store and manage skipped questions so they can be marked in menu', function () {
    menuService.addSkippedQuestion(5);
    expect(menuService.questionSkipped(5)).toBeTruthy();
    menuService.removeSkippedQuestion(5);
    expect(menuService.questionSkipped(5)).toBeFalsy();
  });

});
