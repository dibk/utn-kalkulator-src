'use strict';

describe('Resultrow controller', function () {
  var scope = {};
  var resultFactory;

  beforeEach(module('ngApp'));
  beforeEach(inject(function($controller, ResultFactory) {
    resultFactory = ResultFactory;
    $controller('ResultRowController', {$scope: scope, $ResultFactory: ResultFactory});
  }));

  var returning = function(value) {
    return function() {
      return value;
    };
  };

  it('built area exeeds allowed when it is bigger than allowed', function() {
    resultFactory.allowedArea = returning(100.00);
    resultFactory.sumBuiltArea = returning(100.01);
    expect(scope.builtExceedsAllowed()).toBe(true);

    resultFactory.allowedArea = returning(100.00);
    resultFactory.sumBuiltArea = returning(101);
    expect(scope.builtExceedsAllowed()).toBe(true);
  });

  it('built area wont exeed allowed when it is equal with allowed area', function() {
    resultFactory.allowedArea = returning(100.00);
    resultFactory.sumBuiltArea = returning(100.00);
    expect(scope.builtExceedsAllowed()).toBe(false);

    resultFactory.allowedArea = returning(100.01);
    resultFactory.sumBuiltArea = returning(100.01);
    expect(scope.builtExceedsAllowed()).toBe(false);
  });

  it('built area wont exeed allowed when it is smaller than allowed area', function() {
    resultFactory.allowedArea = returning(100.00);
    resultFactory.sumBuiltArea = returning(99.99);
    expect(scope.builtExceedsAllowed()).toBe(false);

    resultFactory.allowedArea = returning(100);
    resultFactory.sumBuiltArea = returning(99.99);
    expect(scope.builtExceedsAllowed()).toBe(false);
  });


});
