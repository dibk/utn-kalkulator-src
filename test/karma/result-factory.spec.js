'use strict';

describe('Result factory', function () {

  var resultFactory, props, storage, variables;

  beforeEach(function() {
    module('resultFactoryModule', function($provide) {
      storage = {
        propertyArea: 500,
        before2007Regulation: true,
        calculationMethod: 'BYA'
      };
      variables = { 'KEYS' : {
        'm2BRAKey' : 'BRA',
        'pctBRAKey' : '%BRA'
      }};
      $provide.value('$localStorage', storage);
      $provide.value('VARIABLES', variables);
    });
  });

  beforeEach(inject(function (ResultFactory) {
    resultFactory = ResultFactory;

    // Props for BYA
    props = [
      'propertyArea',
      'regulatedToOther',
      'builtResidence',
      'builtGarage',
      'builtSmallBuilding',
      'builtOther',
      'tearDownArea',
      'newRegistredArea',
      'parkingPlaces',
      'parkingPlaceArea',
      'parkingPlacesGarage',
      'before2007Regulation',
      'utilizationPercent',
      'utilizationArea',
      'requiredExtraTerrainParkingPlaces',
      'hasRequiredExtraTerrainParkinPlaces',
      'hasRequiredExtraTerrainParkingPlacesInRegPlan'
    ];

  }));

  it('has utilization % when defined', function() {
    resultFactory.setUtilizationPercent(55);
    expect(resultFactory.isUtilizationPercent()).toBe(true);
  });
  
  it('should have these props', function () {
    for (var i in props) {
      var prop = props[i];
      var hasKey = prop in resultFactory;
      expect(hasKey).toBeTruthy();
    }
    expect(resultFactory.reset).toBeDefined();
    expect(resultFactory.numberOfProperties()).toEqual(props.length);
  });

  it('should be able to load from local storage', function () {
    resultFactory.loadQuestionsFromStorage();
    expect(resultFactory.propertyArea).toEqual(storage.propertyArea);
    expect(resultFactory.before2007Regulation).toEqual(storage.before2007Regulation);
  });

  it('should be able to store to local storage', function () {
    var val = 50;
    var key = 'builtOther';
    resultFactory.setAndPersistProperty(key, val);
    expect(storage[key]).toEqual(val);

    resultFactory.setAndPersistProperty(key, undefined);
    expect(storage[key]).toBeUndefined();
  });

  it('should be able to tell if its empty' ,function () {
    expect(resultFactory.isEmpty()).toBeTruthy();
    expect(resultFactory.numberOfEmptyProperties()).toEqual(resultFactory.numberOfProperties());
  });

  describe('Result factory with values', function () {

    beforeEach(function () {
      storage.unit = 'm2';
      resultFactory.tearDownArea = 10;
      resultFactory.builtResidence = 10;
      resultFactory.builtGarage = 10;
      resultFactory.builtSmallBuilding = 10;
      resultFactory.newRegistredArea = 10;
      resultFactory.hasRequiredExtraTerrainParkingPlacesInRegPlan = false;
      resultFactory.parkingPlaces = 1;
      resultFactory.parkingPlacesGarage = 0;
      resultFactory.parkingPlaceArea = 10;
      resultFactory.before2007Regulation = false;
      resultFactory.utilizationArea = 500;
    });

    it('should not be empty when having values', function () {
      expect(resultFactory.isEmpty()).toBeFalsy();
      expect(resultFactory.numberOfEmptyProperties()).toEqual(6);
      resultFactory.builtOther = 500;
      expect(resultFactory.numberOfEmptyProperties()).toEqual(5);
    });

    it('should be able to tell if result is exceeding allowed area', function () {
      resultFactory.builtOther = 600;
      expect(resultFactory.isExceedingAllowedArea()).toBeTruthy();
      expect(resultFactory.resultType()).toEqual('exceeding');
    });

    it('should be able to tell if result is valid', function () {
      resultFactory.builtOther = 10;
      expect(resultFactory.isValidResult()).toBeTruthy();
      expect(resultFactory.resultType()).toEqual('valid');
    });

    it('should be able to tell if answers is missing', function () {
      
      resultFactory.tearDownArea = undefined;
      expect(resultFactory.isIncomplete()).toBeTruthy();
      expect(resultFactory.resultType()).toEqual('incomplete');
    });

    it('should be completed when there is no empty properties', function () {
      resultFactory.builtOther = 10;
      resultFactory.utilizationPercent = 10;
      resultFactory.calculationMethod = 'BRA';
      resultFactory.parkingPlacesGarage = 1;
      expect(resultFactory.isIncomplete()).toBeFalsy();
    });

    it('Should not be completed when parking area is not filled in when BRA is selected and parkingPlacesGarage>parkingPlaces', function() {
      storage.calculationMethod = 'BRA';
      resultFactory.builtOther = 10;
      resultFactory.parkingPlaces = 3;
      resultFactory.parkingPlacesGarage = 1;
      resultFactory.hasRequiredExtraTerrainParkinPlaces = true;
      resultFactory.requiredExtraTerrainParkingPlaces = 2;
      resultFactory.parkingPlaceArea = undefined;
      expect(resultFactory.isIncomplete()).toBeTruthy();
    });

    it('Should not let user complete without selecting hasRequiredExtraTerrainParkingPlacesInRegPlan', function() {
      storage.calculationMethod = 'BRA';
      resultFactory.builtOther = 10;
      resultFactory.parkingPlaces = 3;
      resultFactory.parkingPlacesGarage = 1;
      resultFactory.hasRequiredExtraTerrainParkingPlacesInRegPlan = undefined;
      resultFactory.parkingPlaceArea = 18;
      expect(resultFactory.isIncomplete()).toBeTruthy();
    });

    it('should let user complete when he has no parkingNorm and no info in hasRequiredExtraTerrainParkingPlacesInRegPlan is false (BRA)', function() {
      storage.calculationMethod = 'BRA';
      resultFactory.builtOther = 10;
      resultFactory.parkingPlaces = 3;
      resultFactory.parkingPlacesGarage = 1;
      resultFactory.hasRequiredExtraTerrainParkingPlacesInRegPlan = true
      resultFactory.requiredExtraTerrainParkingPlaces = undefined;
      resultFactory.parkingPlaceArea = 18;
      expect(resultFactory.isIncomplete()).toBeTruthy();
    });

    it('Should let user have zero parking on terrain when BRA is selected', function() {
      resultFactory.builtOther = 10;
      resultFactory.parkingPlaces = 2;
      resultFactory.parkingPlacesGarage = 2;
      resultFactory.hasRequiredExtraTerrainParkinPlaces = false;
      resultFactory.hasRequiredExtraTerrainParkingPlacesInRegPlan = false;
      resultFactory.parkingPlaceArea = undefined;
      storage.calculationMethod = 'BRA';
      expect(resultFactory.isIncomplete()).toBeFalsy();
    }); 

    it('should not let user leave requiredExtraTerrainParkingPlaces empty when hasRequiredExtraTerrainParkinPlaces is true', function() {
      storage.calculationMethod = 'BRA';
      resultFactory.builtOther = 10;
      resultFactory.parkingPlaces = 2;
      resultFactory.parkingPlacesGarage = 2;
      resultFactory.parkingPlaceArea = 18;
      resultFactory.hasRequiredExtraTerrainParkingPlacesInRegPlan = true;
      resultFactory.requiredExtraTerrainParkingPlaces = undefined;
      expect(resultFactory.isIncomplete()).toBeTruthy();
    });

    it('should not let user leave requiredExtraTerrainParkingPlaces empty when hasRequiredExtraTerrainParkingPlacesInRegPlan is true', function() {
      storage.calculationMethod = 'BRA';
      resultFactory.builtOther = 10;
      resultFactory.parkingPlaces = 2;
      resultFactory.parkingPlacesGarage = 2;
      resultFactory.parkingPlaceArea = 18;
      resultFactory.hasRequiredExtraTerrainParkinPlaces = false;
      resultFactory.hasRequiredExtraTerrainParkingPlacesInRegPlan = true;
      resultFactory.requiredExtraTerrainParkingPlaces = undefined;
      expect(resultFactory.isIncomplete()).toBeTruthy();
    });

    it('should let allow user to continue if he has no parkingNorm and hasRequiredExtraTerrainParkinPlaces is undefined', function() {
      storage.calculationMethod = 'BRA';
      resultFactory.builtOther = 10;
      resultFactory.parkingPlaces = 2;
      resultFactory.parkingPlacesGarage = 2;
      resultFactory.parkingPlaceArea = 18;
      resultFactory.hasRequiredExtraTerrainParkingPlacesInRegPlan = true;
      resultFactory.requiredExtraTerrainParkingPlaces = 2;
      expect(resultFactory.isIncomplete()).toBeFalsy();
    });

    it('should be completed when only empty property is either area or percentage', function () {
      resultFactory.builtOther = 10;
      resultFactory.hasRequiredExtraTerrainParkingPlacesInRegPlan = true;
      resultFactory.requiredExtraTerrainParkingPlaces = 1;
      resultFactory.utilizationPercent = undefined;
      resultFactory.parkingPlacesGarage = 2;

      expect(resultFactory.numberOfEmptyProperties()).toBe(4);
      expect(resultFactory.isIncomplete()).toBeFalsy();

      resultFactory.utilizationPercent = 20;
      resultFactory.utilizationArea= undefined;

      expect(resultFactory.isIncomplete()).toBeFalsy();

      resultFactory.utilizationPercent = 30;
      resultFactory.utilizationArea= 30;

      expect(resultFactory.isIncomplete()).toBeFalsy();

      resultFactory.utilizationPercent = null;
      resultFactory.utilizationArea= undefined;

      expect(resultFactory.isIncomplete()).toBeTruthy();
    });

    it('should be able to tell the type of result', function () {
      resultFactory.builtOther = 300;
      resultFactory.tearDownArea = undefined;
      expect(resultFactory.resultType()).toEqual('incomplete');

      resultFactory.builtResidence = 1110;
      resultFactory.tearDownArea = 10;
      expect(resultFactory.resultType()).toEqual('exceeding');

      resultFactory.builtOther = 10;
      resultFactory.tearDownArea = 10;
      resultFactory.parkingPlacesGarage = undefined;
      expect(resultFactory.resultType()).toEqual('incomplete');

      resultFactory.builtResidence = 1;
      resultFactory.parkingPlacesGarage = 1;
      expect(resultFactory.resultType()).toEqual('valid');
    });

    it('should calculate total area for parking places', function() {
      resultFactory.parkingPlaceArea = 25;
      resultFactory.parkingPlaces = 3;

      expect(resultFactory.sumParkingPlaceArea()).toEqual(75);
    });

  });

  describe('sumParkingPlaceArea', function() {
    it('should return the expected value with BRA selected and no requiredExtraTerrainParkingPlaces', function() {
      storage.calculationMethod = 'BRA';
      storage.parkingNorm = 'no';
      resultFactory.builtOther = 10;
      resultFactory.parkingPlaces = 3;
      resultFactory.parkingPlacesGarage = 2;
      resultFactory.parkingPlaceArea = 18;
      resultFactory.hasRequiredExtraTerrainParkinPlaces = undefined;
      resultFactory.hasRequiredExtraTerrainParkingPlacesInRegPlan = false;
      resultFactory.requiredExtraTerrainParkingPlaces = undefined;
      var parkingResult = 18;
      expect(resultFactory.sumParkingPlaceArea()).toEqual(parkingResult);
    });
    it('should return the expected value with BRA selected and it has hasRequiredExtraTerrainParkingPlacesInRegPlan', function() {
      storage.calculationMethod = 'BRA';
      resultFactory.builtOther = 10;
      resultFactory.parkingPlaces = 3;
      resultFactory.parkingPlacesGarage = 2;
      resultFactory.parkingPlaceArea = 18;
      resultFactory.hasRequiredExtraTerrainParkingPlacesInRegPlan = true;
      resultFactory.requiredExtraTerrainParkingPlaces = 2;
      var parkingResult = 36;
      expect(resultFactory.sumParkingPlaceArea()).toEqual(parkingResult);
    });
    it('should return the expected value with BRA selected and it has no terrain parking', function() {
      storage.calculationMethod = 'BRA';
      storage.parkingNorm = 'no';
      resultFactory.builtOther = 10;
      resultFactory.parkingPlaces = 2;
      resultFactory.parkingPlacesGarage = 2;
      resultFactory.parkingPlaceArea = 18;
      resultFactory.hasRequiredExtraTerrainParkinPlaces = false;
      resultFactory.hasRequiredExtraTerrainParkingPlacesInRegPlan = false;
      resultFactory.requiredExtraTerrainParkingPlaces = 2;
      var parkingResult = 0;
      expect(resultFactory.sumParkingPlaceArea()).toEqual(parkingResult);
    });
  });

  describe('Result factory with BYA', function () {

    beforeEach(function () {
      storage.calculationMethod = 'BYA';
      resultFactory.propertyArea = 300;
      resultFactory.regulatedToOther = 10;
      resultFactory.builtResidence = 10;
      resultFactory.builtGarage = 10;
      resultFactory.builtSmallBuilding = 10;
      resultFactory.tearDownArea = 10;
      resultFactory.newRegistredArea = 10;
      resultFactory.utilizationArea = 500;
    });

    it('should let user specify parking as normal', function() {
      resultFactory.builtOther = 10;
      resultFactory.parkingPlaces = 2;
      resultFactory.hasRequiredExtraTerrainParkingPlacesInRegPlan = false;
      resultFactory.parkingPlacesGarage = 1;
      resultFactory.parkingPlaceArea = 18;
      expect(resultFactory.isIncomplete()).toBeFalsy();
    });

    it('should let user finish with 2 first questions removed', function() {
      storage.unit = 'm2';
      resultFactory.propertyArea = undefined;
      resultFactory.builtOther = 10;
      resultFactory.regulatedToOther = undefined;
      resultFactory.parkingPlaces = 2;
      resultFactory.parkingPlaceArea = 18;
      resultFactory.hasRequiredExtraTerrainParkingPlacesInRegPlan = false;
      resultFactory.parkingPlacesGarage = 1;
      expect(resultFactory.isIncomplete()).toBeFalsy();
    });

  });

  describe('Result factory with random-ish values', function () {

    beforeEach(function () {
      for (var i in props) {
        var prop = props[i];
        if (typeof resultFactory[prop] === 'boolean') {
          resultFactory[prop] = true;
        } else {
          resultFactory[prop] = 99;
        }
      }
    });

    it('should be able to reset', function () {
      resultFactory.reset();
      for (var i in props) {
        var prop = props[i];
        if (typeof resultFactory[prop] === 'boolean') {
          expect(resultFactory[prop]).toBeFalsy();
        } else {
          expect(resultFactory[prop]).toBeUndefined();
        }
      }
    });

  });

});