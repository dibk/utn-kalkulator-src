'use strict';

xdescribe('Intro controller', function () {

  var resultFactory, introFactory, persistPropertySpy;
  var scope = {};

  beforeEach(module('ngApp'));
  beforeEach(module('resultFactoryModule'));
  beforeEach(module('introFactoryModule'));

  beforeEach(inject(function($controller, ResultFactory, IntroFactory) {

    resultFactory = ResultFactory;
    introFactory = IntroFactory;

    persistPropertySpy = spyOn(introFactory, 'persistProperty');

    $controller('IntroController', {
      $scope: scope,
      ResultFactory: resultFactory
    });

  }));

  it('should by default hide the confirm modal', function () {
    expect(scope.confirmModalVisibility).toEqual('hide');
  });

  it('should presist properties when they have been updated', function () {
    var key = 'regulated';
    scope.introPropertyUpdated(key);
    expect(persistPropertySpy.calls.mostRecent().args[0]).toEqual(key);
  });

  describe('when result-factory has got some values', function () {

    beforeEach(function () {

      resultFactory.propertyArea = 300;
      resultFactory.tearDownArea = 50;
      resultFactory.utilizationArea = 500;

    });

    it('should want to show the confirm modal when result-factory got values', function () {
      var key = 'regulated';
      scope.introPropertyUpdated(key);
      expect(introFactory.persistProperty).not.toHaveBeenCalled();
      expect(scope.confirmModalVisibility).toEqual('show');
    });

    it('should revert the value when confirmation is canceled', function () {
      var key = 'regulated';
      introFactory[key] = true;
      scope.introPropertyUpdated(key, 'false');
      expect(scope.confirmModalVisibility).toEqual('show');
      scope.cancelConfirmChangeModal();
      expect(introFactory[key]).toEqual(false);
    });

    it('should restart result-factory values that is not set from intro on restart', function () {
      var key = 'regulated';
      introFactory[key] = true;
      scope.introPropertyUpdated(key, 'false');
      expect(scope.confirmModalVisibility).toEqual('show');
      scope.confirmChangeModal();
      expect(introFactory[key]).toEqual(true);
      expect(resultFactory.numberOfEmptyProperties()).toEqual(resultFactory.numberOfProperties());
      expect(resultFactory.propertyArea).toBeUndefined();
      expect(resultFactory.tearDownArea).toBeUndefined();
      expect(resultFactory.utilizationArea).toBeUndefined();
    });

  });

});
