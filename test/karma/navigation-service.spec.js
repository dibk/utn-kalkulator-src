'use strict';

describe('NavigationService', function () {
  var navigationService;

  var qService = {
    findByNumber : function() {}
  };

  beforeEach(module('ngApp', function($provide) {
    $provide.value('QuestionService', qService);
  }));

  beforeEach(module('navigationServiceModule'));
  beforeEach(inject(function (NavigationService) {
    navigationService = NavigationService;
  }));


  it('should return next question number when number is available', inject(function (QuestionService) {
    spyOn(QuestionService, "findByNumber").and.returnValue({next: 2});
    expect(navigationService.structuralNext(1)).toBe('/question/2');
  }));

  it('should return next URL when number is not available', inject(function (QuestionService) {
    spyOn(QuestionService, "findByNumber").and.returnValue({next: "/finish"});
    expect(navigationService.structuralNext(1)).toBe('/finish');
  }));

  it('should return previous question number when number is available', inject(function (QuestionService) {
    spyOn(QuestionService, "findByNumber").and.returnValue({previous: 2});
    expect(navigationService.structuralPrevious(3)).toBe('/question/2');
  }));

  it('should return previous URL when number is not available', inject(function (QuestionService) {
    spyOn(QuestionService, "findByNumber").and.returnValue({previous: '/intro'});
    expect(navigationService.structuralPrevious(3)).toBe('/intro');
  }));
});
