'use strict';

describe('Intro Factory', function () {

  var introFactory, props, storage;

  beforeEach(function () {
    module('introFactoryModule', function($provide) {

      storage = {
        regulated: true,
        unit: 'm2',
        utilizationArea: '122'
      };

      $provide.value('$localStorage', storage);
    });
  });

  beforeEach(inject(function(IntroFactory) {
    introFactory = IntroFactory;

    props = [
      'showOverridingQuestion',
      'showError',
      'regulated',
      'decision',
      'regulationTime',
      'regulationPlan',
      'unit',
      'utilizationArea'
    ];

  }));

  it('should have these props', function () {
    for (var i in props) {
      var prop = props[i];
      var hasKey = prop in introFactory;
      expect(hasKey).toBeTruthy();
    }
    expect(introFactory.reset).toBeDefined();
  });

  it('should be able to load from local storage', function () {
    introFactory.loadIntroFromStorage();
    expect(introFactory.regulated).toEqual(storage.regulated);
    expect(introFactory.unit).toEqual(storage.unit);
    expect(introFactory.utilizationArea).toEqual(storage.utilizationArea);
  });

  it('should be able to store to local storage', function () {
    introFactory.showError = true;
    introFactory.persistProperty('showError');
    expect(storage.showError).toBeTruthy();
    introFactory.setAndPersistProperty('showError', undefined);
    expect(storage.showError).toBeUndefined();
  });

  describe('Intro Factory with values', function () {

    beforeEach(function () {
      for (var i in props) {
        var prop = props[i];
        if (typeof introFactory[prop] === 'boolean') {
          introFactory[prop] = true;
        } else {
          introFactory[prop] = 99;
        }
      }
    });

    it('should be able to reset', function () {
      introFactory.reset();
      for (var i in props) {
        var prop = props[i];
        if (typeof introFactory[prop] === 'boolean') {
          expect(introFactory[prop]).toBeFalsy();
        } else {
          expect(introFactory[prop]).toBeUndefined();
        }
      }
    });

  });

});