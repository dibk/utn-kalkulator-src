'use strict';

var IntroPage = function () {

  this.radioNotRegulated = element(by.id('notRegulated'));
  this.radioRegulated = element(by.id('regulated'));
  this.radioNotDecision = element(by.id('notDecision'));
  this.radioDecision = element(by.id('overridingDecision'));
  this.radioRegulationAfter = element(by.id('regulationAfter'));
  this.radioPercent = element(by.id('percent'));
  this.radioAbsolute = element(by.id('absolute'));

  this.warningText = element(by.css('div.text-warning'));
  this.utilizationInput = element(by.id('utilizationAreaInput'));

  this.get = function () {
    browser.get('http://localhost:3000/#/intro');
  };

  this.headers = function() {
    return element.all(by.css('h3'));
  };

  this.validationFailures = function() {
    return element.all(by.css('.invalid-text')).filter(function(elem) {
      return elem.isDisplayed();
    });
  };

};

module.exports = new IntroPage();
