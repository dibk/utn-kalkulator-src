'use strict';

describe('The intro page', function () {
  var introPage = require('./intro.po');

  function proceedToUtilization() {
    introPage.radioRegulated.click();
    introPage.radioDecision.click();
    introPage.radioRegulationAfter.click();
    introPage.radioPercent.click();
  }

  beforeEach(function () {
    introPage.get();
  });

  afterEach(function () {
    browser.executeScript('window.localStorage.clear();');
  });

  it('should show warning when not regulated nor municipality decision', function () {
    introPage.radioNotRegulated.click();
    introPage.radioNotDecision.click();

    expect(introPage.warningText.isPresent()).toBe(true);
  });

  it('should show lable for municipality decision, when it overrides regulation', function () {
    introPage.radioRegulated.click();
    introPage.radioDecision.click();

    expect(introPage.headers().get(3).getText()).toBe('Når er kommuneplanen vedtatt?');
  });

  it('should show utilization input as last element', function () {
    proceedToUtilization();

    expect(introPage.utilizationInput.isDisplayed()).toBe(true);
  });

  it('should validate 50 as utilization percent', function () {
    proceedToUtilization();
    introPage.utilizationInput.sendKeys('50');

    expect(introPage.validationFailures().count()).toBe(0);
  });

  it('should clear the utilization input on utilization type change', function () {
    proceedToUtilization();
    introPage.utilizationInput.sendKeys('200');
    introPage.utilizationInput.getAttribute('value').then(function (value) {
      expect(value).toBe('200');
    });

    introPage.radioAbsolute.click();

    introPage.utilizationInput.getAttribute('value').then(function (value) {
      expect(value).toBe('');
    });
  });

});
