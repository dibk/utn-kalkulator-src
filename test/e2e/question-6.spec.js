'use strict';

describe('Question 6 page', function () {
  var question6Page = require('./questions.po');
  var inputs = question6Page.inputs(['parkingPlaceArea', 'parkingPlaces']);

  var expectInvalid = function (message) {
    expect(question6Page.validationFailures().count()).toBe(1);
    expect(question6Page.validationFailures().get(0).getText()).toBe(message);
  };

  beforeEach(function () {
    question6Page.get(6);
  });

  it('should not validate input which is not a number', function () {
    inputs.forEach(function (input) {
      ['-', '.'].forEach(function (keys) {
        input.sendKeys(keys);
        expectInvalid('Benytt kun tall');
        question6Page.get(6);
      });
    });
  });

  it('should not validate too big numbers', function () {
    inputs.forEach(function (input) {
      input.sendKeys('126456');
      expectInvalid('For stort tall');
      question6Page.get(6);
    });
  });

  it('should move to result page by clicking next', function () {
    question6Page.nextLink.click();
    expect(browser.getCurrentUrl()).toContain('/#/result');
  });

});
