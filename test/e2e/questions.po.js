'use strict';

var QuestionsPage = function () {

  this.nextLink = element(by.linkText('Neste >'));

  this.get = function (number) {
    browser.get('http://localhost:3000/#/question/' + number);
  };

  this.validationFailures = function() {
    return element.all(by.css('.invalid-text')).filter(function(elem) {
      return elem.isDisplayed();
    });
  };

  this.inputs = function (inputIds) {
    return inputIds.map(function(id) {
      return element(by.id(id));
    });
  };

  this.input = function (inputId) {
    return element(by.id(inputId));
  };

};

module.exports = new QuestionsPage();
