'use strict';

describe('Question 5 page', function () {
  var question5Page = require('./questions.po');
  var newRegistredArea = question5Page.input('newRegistredAreaInput');

  var expectInvalid = function (message) {
    expect(question5Page.validationFailures().count()).toBe(1);
    expect(question5Page.validationFailures().get(0).getText()).toBe(message);
  };

  beforeEach(function () {
    question5Page.get(5);
  });

  it('should not validate input which is not a number', function() {
    ['-', '.'].forEach(function(keys) {
      newRegistredArea.sendKeys(keys);
      expectInvalid('Benytt kun tall');
      question5Page.get(5);
    });
  });

  it('should not validate too big numbers', function() {
    newRegistredArea.sendKeys('153556');
    expectInvalid('For stort tall')
  });

  it('should move to question 6 by clicking next', function() {
    question5Page.nextLink.click();
    expect(browser.getCurrentUrl()).toContain('/#/question/6');
  });

});
