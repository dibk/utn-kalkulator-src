'use strict';

describe('Question 3 page', function () {
  var question3Page = require('./questions.po');
  var inputs = question3Page.inputs(['builtResidence', 'builtOther', 'builtSmallBuilding', 'builtGarage']);

  var expectInvalid = function (message) {
    expect(question3Page.validationFailures().count()).toBe(1);
    expect(question3Page.validationFailures().get(0).getText()).toBe(message);
  };

  beforeEach(function () {
    question3Page.get(3);
  });

  it('should not validate input which is not a number', function () {
    inputs.forEach(function (input) {
      ['-', '.'].forEach(function (keys) {
        input.sendKeys(keys);
        expectInvalid('Benytt kun tall');
        question3Page.get(3);
      });
    });
  });

  it('should not validate too big numbers', function () {
    inputs.forEach(function (input) {
      input.sendKeys('123456');
      expectInvalid('For stort tall');
      question3Page.get(3);
    });
  });

  it('should set zero to all fields by clicking checkbox', function () {
    question3Page.input('resetAreal').click();
    inputs.forEach(function (input) {
      input.getAttribute('value').then(function(value) {
        expect(value).toBe('0');
      })
    });
  });

  it('should move to question 4 by clicking next', function () {
    question3Page.nextLink.click();
    expect(browser.getCurrentUrl()).toContain('/#/question/4');
  });

});
