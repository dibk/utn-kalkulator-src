'use strict';

describe('Question 1 page', function () {
  var question1Page = require('./questions.po');
  var propertyArea = question1Page.input('propertyAreaInput');

  var expectInvalid = function (message) {
    expect(question1Page.validationFailures().count()).toBe(1);
    expect(question1Page.validationFailures().get(0).getText()).toBe(message);
  };

  beforeEach(function () {
    question1Page.get(1);
  });

  it('should not validate input which is not a number', function() {
    ['-', '.'].forEach(function(keys) {
      propertyArea.sendKeys(keys);
      expectInvalid('Benytt kun tall');
      question1Page.get(1);
    });
  });

  it('should not validate too big numbers', function() {
    propertyArea.sendKeys('123456');
    expectInvalid('For stort tall')
  });

  it('should validate only 2 decimals', function() {
    propertyArea.sendKeys('12.345');
    expectInvalid('Benytt punktum ved bruk av desimaler. Kun to desimaler.')
  });

  it('should move to question 2 by clicking next', function() {
    question1Page.nextLink.click();
    expect(browser.getCurrentUrl()).toContain('/#/question/2');
  });

});
