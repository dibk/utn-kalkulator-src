'use strict';

var StartPage = function() {
  var scrollDown = 'window.scrollTo(0,1000);';

  this.startButton = element(by.linkText('Start'));

  this.cookieInfo = element(by.linkText('Les mer om vår bruk av cookies her.'));

  this.closeCookieInfo = element(by.css('.cookie-info-close'));

  this.cookieInfoBar = element(by.css('.cookie-info-wrapper'));

  this.get = function() {
    browser.get('http://localhost:3000/#/start');
  };

  this.title = function() {
    return browser.getTitle();
  };

  this.clickStart = function() {
    var that = this;
    browser.executeScript(scrollDown).then(function () {
      that.startButton.click();
    });
  };

};

module.exports = new StartPage();
