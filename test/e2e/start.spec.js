'use strict';

describe('The start page', function () {
  var startPage = require('./start.po');

  beforeEach(function () {
    startPage.get();
  });

  afterEach(function () {
    browser.executeScript('window.localStorage.clear();');
  });

  it('should have correct title', function() {
    expect(startPage.title()).toBe('Hvor stort kan du bygge?');
  });

  it('should move to intro after start button is clicked', function() {

    startPage.clickStart();

    expect(browser.getCurrentUrl()).toContain('/#/intro');
  });

  it('should move to cookie info when link is clicked', function() {
    startPage.cookieInfo.click();

    expect(browser.getCurrentUrl()).toContain('/#/cookieinfo');
  });

  it('should close cookie info when close button is clicked', function() {
    startPage.closeCookieInfo.click();

    expect(startPage.cookieInfoBar.isPresent()).toBe(false);
  });

});
