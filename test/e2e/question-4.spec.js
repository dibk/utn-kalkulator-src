'use strict';

describe('Question 4 page', function () {
  var question4Page = require('./questions.po');
  var tearDownArea = question4Page.input('tearDownAreaInput');

  var expectInvalid = function (message) {
    expect(question4Page.validationFailures().count()).toBe(1);
    expect(question4Page.validationFailures().get(0).getText()).toBe(message);
  };

  beforeEach(function () {
    question4Page.get(4);
  });

  it('should not validate input which is not a number', function() {
    ['-', '.'].forEach(function(keys) {
      tearDownArea.sendKeys(keys);
      expectInvalid('Benytt kun tall');
      question4Page.get(4);
    });
  });

  it('should not validate too big numbers', function() {
    tearDownArea.sendKeys('143456');
    expectInvalid('For stort tall')
  });

  it('should move to question 5 by clicking next', function() {
    question4Page.nextLink.click();
    expect(browser.getCurrentUrl()).toContain('/#/question/5');
  });

});
