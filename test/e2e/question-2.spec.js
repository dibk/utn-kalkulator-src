'use strict';

describe('Question 2 page', function () {
  var question2Page = require('./questions.po');
  var regulatedToOther = question2Page.input('regulatedToOtherInput');

  var expectInvalid = function (message) {
    expect(question2Page.validationFailures().count()).toBe(1);
    expect(question2Page.validationFailures().get(0).getText()).toBe(message);
  };

  beforeEach(function () {
    question2Page.get(2);
  });

  it('should not validate input which is not a number', function() {
    ['-', '.'].forEach(function(keys) {
      regulatedToOther.sendKeys(keys);
      expectInvalid('Benytt kun tall');
      question2Page.get(2);
    });
  });

  it('should not validate too big numbers', function() {
    regulatedToOther.sendKeys('123456');
    expectInvalid('For stort tall')
  });

  it('should move to question 3 by clicking next', function() {
    question2Page.nextLink.click();
    expect(browser.getCurrentUrl()).toContain('/#/question/3');
  });

});
