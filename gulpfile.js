'use strict';

var gulp = require('gulp'),
  gutil = require('gulp-util'),
  gulpNgConfig = require('gulp-ng-config'),
  autoprefixer = require('gulp-autoprefixer'),
  del = require('del'),
  csso = require('gulp-csso'),
  gulpFilter = require('gulp-filter'),
  flatten = require('gulp-flatten'),
  jshint = require('gulp-jshint'),
  size = require('gulp-size'),
  uglify = require('gulp-uglify'),
  useref = require('gulp-useref'),
  ngAnnotate = require('gulp-ng-annotate'),
  replace = require('gulp-replace'),
  inject = require('gulp-inject'),
  rev = require('gulp-rev'),
  revReplace = require('gulp-rev-replace'),
  minifyHtml = require('gulp-minify-html'),
  protractor = require('gulp-protractor'),
  less = require('gulp-less'),
  browserSync = require('browser-sync'),
  browserSyncSpa = require('browser-sync-spa'),
  wiredep = require('wiredep'),
  karma = require('karma'),
  concat = require('concat-stream'),
  _ = require('lodash'),
  htmlhint = require("gulp-htmlhint");

var environment = 'dev';
var config_file = 'config.json';
var ga_template = 'google-analytics.tpl';
var paths = {
  src: 'src',
  bower: 'bower_components',
  test: 'test/karma',
  dist: 'dist',
  tmp: '.tmp',
  serve: '.tmp/serve',
  e2e: 'test/e2e'
};

var errorHandler = function (title) {
  return function (err) {
    gutil.log(gutil.colors.red('[' + title + ']'), err.toString());
    this.emit('end');
    process.exit(1);
  };
};

var runProtractor = function (done) {
  var specs = [
    paths.e2e + '/start.spec.js',
    paths.e2e + '/intro.spec.js',
    paths.e2e + '/question-1.spec.js',
    paths.e2e + '/question-2.spec.js',
    paths.e2e + '/question-3.spec.js',
    paths.e2e + '/question-4.spec.js',
    paths.e2e + '/question-5.spec.js',
    paths.e2e + '/question-6.spec.js'
  ];

  gulp.src(specs)
    .pipe(protractor.protractor({
      configFile: 'protractor.conf.js'
    }))
    .on('error', function () {
      browserSync.exit(-1);
    })
    .on('end', function () {
      browserSync.exit();
      done();
    });
};


var browserSyncInit = function (baseDir) {
  browserSync.instance = browserSync.init({
    startPath: '/',
    server: {
      baseDir: baseDir,
      routes: {
        '/bower_components': 'bower_components'
      }
    }
  });
};
var listFiles = function (callback) {
  var bowerDeps = wiredep({
    directory: 'bower_components',
    exclude: [/jquery/, /bootstrap\.js/, /bootstrap\.css/],
    dependencies: true,
    devDependencies: true
  });

  var karmaSpecFiles = paths.test + '/**/*.spec.js';
  var htmlFiles = paths.src + '/**/*.html';
  var srcFiles = paths.src + '/**/*.js';

  gulp.src(srcFiles)
    .pipe(concat(function (files) {
      callback(bowerDeps.js
        .concat(_.pluck(files, 'path'))
        .concat(htmlFiles)
        .concat(karmaSpecFiles));
    }));
};

var runTests = function (singleRun, done) {
  listFiles(function (files) {
    karma.server.start({
      configFile: __dirname + '/karma.conf.js',
      files: files,
      singleRun: singleRun
    }, done);
  });
};

browserSync.use(browserSyncSpa({
  selector: '[ng-app]'// Only needed for angular apps
}));

gulp.task('html', ['injectTmp'], function () {
  var htmlFiles = gulpFilter('**/*.html');
  var jsFiles = gulpFilter('**/*.js');
  var cssFiles = gulpFilter('**/*.css');
  var assets = useref.assets();

  return gulp.src(paths.tmp + '/serve/**/*.html')
    .pipe(assets)
    .pipe(rev())

    .pipe(jsFiles).pipe(ngAnnotate())
    .pipe(uglify({preserveComments: false})).on('error', errorHandler('Uglify'))
    .pipe(jsFiles.restore())

    .pipe(cssFiles).pipe(replace('../../bower_components/bootstrap/fonts/', '../font/')).pipe(csso())
    .pipe(cssFiles.restore())
    .pipe(assets.restore())

    .pipe(useref()).pipe(revReplace())
    .pipe(htmlFiles).pipe(minifyHtml())
    .pipe(htmlFiles.restore())

    .pipe(gulp.dest(paths.dist + '/'))
    .pipe(size({title: paths.dist + '/', showFiles: true}));
});

gulp.task('injectTmp', ['scripts', 'styles'], function () {
  return gulp.src(paths.src + '/**/*.html').
    pipe(inject(gulp.src(['./src/' + ga_template]), {
      starttag: '<!-- inject:google-analytics -->',
      transform: function (filePath, file) {
        return file.contents.toString('utf8')
      }
    }))
    .pipe(htmlhint())
    .pipe(htmlhint.failReporter()).on('error', errorHandler("Html validation."))
    .pipe(gulp.dest(paths.serve));
});

gulp.task('scripts', ['hotJar'], function () {
  return gulp.src([paths.src + '/**/*.js', '!' + paths.src + '/config.js', '!**/*/hotjar.js'])
    .pipe(jshint()).pipe(jshint.reporter('jshint-stylish'))
    .pipe(gulp.dest(paths.serve))
    .pipe(browserSync.reload({stream: true}))
    .pipe(size({showFiles: true}));
});

gulp.task('hotJar', function () {
  var resultFolder = '/components/result/';
  return gulp.src(paths.src + resultFolder + 'hotjar.js')
    .pipe(gulp.dest(paths.dist + resultFolder))
    .pipe(gulp.dest(paths.serve + resultFolder));
});

gulp.task('styles', function () {
  return gulp.src([paths.src + '/assets/less/*.less'])
    .pipe(less()).on('error', errorHandler('Less'))
    .pipe(autoprefixer()).on('error', errorHandler('Autoprefixer'))
    .pipe(gulp.dest(paths.tmp + '/serve/app/'))
    .pipe(browserSync.reload({stream: true}))
    .pipe(size({showFiles: true}))
});

gulp.task('fonts', function () {
  return gulp.src([paths.src + '/assets/font/*.{woff,ttf,eot,svg}', paths.bower + '/bootstrap/fonts/*'])
    .pipe(flatten())
    .pipe(gulp.dest(paths.dist + '/font/'))
    .pipe(gulp.dest(paths.tmp + '/serve/font/'));
});

gulp.task('images', function () {
  return gulp.src(paths.src + '/assets/images/*.{jpg,png}')
    .pipe(gulp.dest(paths.dist + '/images/'))
    .pipe(gulp.dest(paths.tmp + '/serve/images/'));
});

gulp.task('config', function () {
  return gulp.src(paths.src + '/' + config_file)
    .pipe(replace('%TIMESTAMP%', new Date()))
    .pipe(gulpNgConfig('config', {environment: environment}))
    .pipe(gulp.dest(paths.src))
    .pipe(gulp.dest(paths.serve));
});

gulp.task('watch', ['injectTmp'], function () {

  gulp.watch(paths.src + '/**/*.less', function () {
    gulp.start('styles');
  });

  gulp.watch(paths.src + '/**/*.js', function () {
    gulp.start('scripts');
  });

  gulp.watch(paths.src + '/**/*.html', function () {
    gulp.src(paths.src + '/**/*.html')
      .pipe(gulp.dest(paths.serve))
      .pipe(browserSync.reload({stream: true}));
  });
});

gulp.task('serve', ['build', 'watch'], function () {
  browserSyncInit([paths.serve, paths.src]);
});

gulp.task('serve:dist', ['build'], function () {
  browserSyncInit(paths.dist);
});

gulp.task('serve:e2e', ['injectTmp'], function () {
  browserSyncInit([paths.serve, paths.src], []);
});

gulp.task('serve:e2e-dist', ['build'], function () {
  browserSyncInit(paths.dist, []);
});

gulp.task('other', function () {
  return gulp.src(['Web.config', paths.src + '/**/*', '!' + paths.src + '/**/*.{html,css,js,less}'])
    .pipe(gulp.dest(paths.dist + '/'));
});

gulp.task('test', ['scripts'], function (done) {
  runTests(true, done);
});

gulp.task('debug-test', ['scripts'], function (done) {
  runTests(false, done);
});

gulp.task('test:auto', ['watch'], function (done) {
  runTests(false, done);
});

gulp.task('webdriver-update', protractor.webdriver_update);

gulp.task('webdriver-standalone', protractor.webdriver_standalone);

gulp.task('protractor', ['protractor:src']);

gulp.task('protractor:src', ['serve:e2e', 'webdriver-update'], runProtractor);

gulp.task('protractor:dist', ['serve:e2e-dist', 'webdriver-update'], runProtractor);

gulp.task('clean', function (done) {
  del([paths.dist + '/', paths.tmp + '/'], done);
});

gulp.task('build', ['config','images', 'html', 'fonts', 'other'], function (done) {
  runTests(true, done);
  del([paths.dist + '/' + config_file, paths.dist + '/' + ga_template]);
});

gulp.task('build-staging', ['clean'], function () {
  environment = 'staging';
  gulp.start('build');
});

gulp.task('build-prod', ['clean'], function () {
  environment = 'prod';
  gulp.start('build');
});

gulp.task('build-dev', ['clean'], function () {
  environment = 'dev';
  gulp.start('build');
});

gulp.task('default', ['clean'], function () {
  gulp.start('build');
});
